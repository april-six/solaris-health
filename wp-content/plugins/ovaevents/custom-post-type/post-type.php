<?php

// Slideshow //////////////////////////////////////////
add_action( 'init', 'slideshow_init',0 );
function slideshow_init() {
    
    $labels = array(
        'name'               => __( 'Slideshows', 'post type general name', 'events' ),
        'singular_name'      => __( 'Slide', 'post type singular name', 'events' ),
        'menu_name'          => __( 'Slideshows', 'admin menu', 'events' ),
        'name_admin_bar'     => __( 'Slide', 'add new on admin bar', 'events' ),
        'add_new'            => __( 'Add New slide', 'Slide', 'events' ),
        'add_new_item'       => __( 'Add New Slide', 'events' ),
        'new_item'           => __( 'New Slide', 'events' ),
        'edit_item'          => __( 'Edit Slide', 'events' ),
        'view_item'          => __( 'View Slide', 'events' ),
        'all_items'          => __( 'All Slides', 'events' ),
        'search_items'       => __( 'Search Slides', 'events' ),
        'parent_item_colon'  => __( 'Parent Slides:', 'events' ),
        'not_found'          => __( 'No Slides found.', 'events' ),
        'not_found_in_trash' => __( 'No Slides found in Trash.', 'events' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'menu_icon'          => 'dashicons-format-gallery',
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'slideshow' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail','comments'),
        'taxonomies'          => array('slidegroup'),
    );

    register_post_type( 'slideshow', $args );
}


add_action( 'init', 'create_slidegroup_taxonomies', 0 );
// create slidegroup taxonomy
function create_slidegroup_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => __( 'Group', 'taxonomy general name' , 'events'),
        'singular_name'     => __( 'Group', 'taxonomy singular name' , 'events'),
        'search_items'      => __( 'Search Group', 'events'),
        'all_items'         => __( 'All Group', 'events' ),
        'parent_item'       => __( 'Parent Group', 'events' ),
        'parent_item_colon' => __( 'Parent Group:' , 'events'),
        'edit_item'         => __( 'Edit Group' , 'events'),
        'update_item'       => __( 'Update Group' , 'events'),
        'add_new_item'      => __( 'Add New Group' , 'events'),
        'new_item_name'     => __( 'New Group Name' , 'events'),
        'menu_name'         => __( 'Group' , 'events'),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'slideshow' )
    );

    register_taxonomy( 'slidegroup', array('slideshow'), $args );
}







// Schedule /////////////////////////////////////////////////////////
add_action( 'init', 'schedule_post_type', 0 );
function schedule_post_type() {

    $labels = array(
        'name'                => __( 'Schedule', 'Post Type General Name', 'events' ),
        'singular_name'       => __( 'Schedule', 'Post Type Singular Name', 'events' ),
        'menu_name'           => __( 'Schedule', 'events' ),
        'parent_item_colon'   => __( 'Parent Schedule:', 'events' ),
        'all_items'           => __( 'All Schedules', 'events' ),
        'view_item'           => __( 'View Schedule', 'events' ),
        'add_new_item'        => __( 'Add New Schedule', 'events' ),
        'add_new'             => __( 'Add New Schedule', 'events' ),
        'edit_item'           => __( 'Edit Schedule', 'events' ),
        'update_item'         => __( 'Update Schedule', 'events' ),
        'search_items'        => __( 'Search Schedules', 'events' ),
        'not_found'           => __( 'No Schedules found', 'events' ),
        'not_found_in_trash'  => __( 'No Schedules found in Trash', 'events' ),
    );
    $args = array(
        'label'               => __( 'schedule', 'events' ),
        'description'         => __( 'Schedule information pages', 'events' ),
        'labels'              => $labels,
        'supports'            => array( 'thumbnail', 'editor', 'title', 'comments','excerpt'),
        'taxonomies'          => array('categories'),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'menu_icon'          => 'dashicons-calendar',
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,        
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'schedule', $args );
}

add_action( 'init', 'create_schedule_taxonomies', 0 );
// create categories taxonomy
function create_schedule_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => __( 'Categories', 'taxonomy general name' , 'events'),
        'singular_name'     => __( 'Categories', 'taxonomy singular name' , 'events'),
        'search_items'      => __( 'Search Categories', 'events'),
        'all_items'         => __( 'All Categories', 'events' ),
        'parent_item'       => __( 'Parent Category', 'events' ),
        'parent_item_colon' => __( 'Parent Category:' , 'events'),
        'edit_item'         => __( 'Edit Category' , 'events'),
        'update_item'       => __( 'Update Category' , 'events'),
        'add_new_item'      => __( 'Add New Category' , 'events'),
        'new_item_name'     => __( 'New Category Name' , 'events'),
        'menu_name'         => __( 'Categories' , 'events'),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'schedule' )        
    );

    register_taxonomy( 'categories', array('schedule'), $args );
}

