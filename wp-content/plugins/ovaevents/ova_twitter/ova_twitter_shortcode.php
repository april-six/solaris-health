<?php


require_once(dirname(__FILE__) . '/twitteroauth.php');


function getAgoshortcode($timestamp) {
        $difference = time() - $timestamp;

        if ($difference < 60) {
            return $difference.esc_html__(' seconds ago','events');
        } else {
            $difference = round($difference / 60);
        }

        if ($difference < 60) {
            return $difference.esc_html__(" minutes ago","events");
        } else {
            $difference = round($difference / 60);
        }

        if ($difference < 24) {
            return $difference.esc_html__(" hours ago","events");
        }
        else {
            $difference = round($difference / 24);
        }

        if ($difference < 7) {
            return $difference.esc_html__(" days ago","events");
        } else {
            $difference = round($difference / 7);
            return $difference.esc_html__(" weeks ago","events");
        }
}

function getConnectionWithAccessTokenshortcode($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
  $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
  return $connection;
}

?>
