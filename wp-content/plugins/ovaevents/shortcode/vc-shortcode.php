<?php
add_action('init','init_visual_composer_custom',1000);
function init_visual_composer_custom(){
    if(function_exists('vc_map')){

/* Main slider */

$args_slidegroup = array(
  'type'                     => 'post',
  'child_of'                 => 0,
  'parent'                   => '',
  'orderby'                  => 'slug',
  'order'                    => 'ASC',
  'hide_empty'               => 1,
  'hierarchical'             => 1,
  'exclude'                  => '',
  'include'                  => '',
  'number'                   => '',
  'taxonomy'                 => 'slidegroup',
  'pad_counts'               => false 

);
$categories_slidegroup = get_categories($args_slidegroup);
$cate_array_slidegroup = array();
$empty_slidegroup = array("Select slide category" => "");
if ($categories_slidegroup) {
	foreach ( $categories_slidegroup as $cate ) {
		$cate_array_slidegroup[$cate->cat_name] = $cate->slug;
	}
} else {
	$cate_array_slidegroup["No content Category found"] = 0;
}


vc_map( array(
	 "name" => __("Main slider", 'events'),
	 "base" => "events_main_slider",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Choose a slideshow category",'events'),
	       "description" => __("Display categories that exists item. You need to add item in Slideshows >> Add new slide",'events'),
	       "value" => array_merge($empty_slidegroup,$cate_array_slidegroup),
	       "param_name" => "slug_group",
	       "default"	=> "",
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("order by",'events'),
	       "param_name" => "order_by",
	       "value" => array(   
		            __('id', 'events') => 'id',
		            __('slug', 'events') => 'slug',
		            __('date', 'events') => 'date',
		            __('modified', 'events') => 'modified',
		            __('rand', 'events') => 'rand',
		            ),
		    "default" => 'id'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Order",'events'),
	       "param_name" => "order",
	       "value" => array(
	       		__('DESC', 'events') => "DESC",
	       		__('ASC', 'events') => "ASC",
	       	),
	       "default"	=> "DESC"
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Count slide",'events'),
	       "param_name" => "count",
	       "value"	=> "100"
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Auto slider",'events'),
	       "param_name" => "auto_slider",
	       "value" => array(
	       		__('True', 'events') => "true",
	       		__('False', 'events') => "false",
	       	),
	       "default"	=> "true"
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Duration of slider(ms). 1000ms = 1s",'events'),
	       "param_name" => "duration",
	       "value"	=> '3000'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Navigation",'events'),
	       "param_name" => "navigation",
	       "value" => array(
	       		__('True', 'events') => "true",
	       		__('False', 'events') => "false",
	       	),
	       "default"	=> "true"
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Loop",'events'),
	       "param_name" => "loop",
	       "value" => array(
	       		esc_html__('True', 'events') => "true",
	       		esc_html__('False', 'events') => "false",
	       	),
	       "default"	=> "true"
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => esc_html__("Display cover background",'events'),
	       "param_name" => "cover_bg",
	       "value" => array(
	       		__('True', 'events') => "true",
	       		__('False', 'events') => "false",
	       	),
	       "default"	=> "true"
	    ),
		array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => "",
	       "description" => esc_html__('Insert class to use for your style','events')
	    )

	 
)));

vc_map( array(
	 "name" => __("Button", 'events'),
	 "base" => "events_button",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Name",'events'),
	       "param_name" => "name",
	       "value" => ""
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Link",'events'),
	       "param_name" => "link",
	       "value" => ""
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Target",'events'),
	       "param_name" => "target",
	       "value" => array(
	       		__('Scroll', 'events') => "scroll",
	       		__('Blank', 'events') => "_blank",
	       		__('Parent', 'events') => "_parent",
	       		__('Top', 'events') => "_top",
	       		__('Popup Video', 'events') => "popup_video",
	       	),
	       "default"	=> "scroll"
	    ),
	    array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Background color",'events'),
	       "param_name" => "bg",
	    ),
	    array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Background color hover",'events'),
	       "param_name" => "bg_hover",
	    ),
	    array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Text color",'events'),
	       "param_name" => "text_color",
	    ),
	    array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Text color hover",'events'),
	       "param_name" => "text_color_hover",
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Icon",'events'),
	       "param_name" => "icon",
	       "value" => "",
	       "description" => 'Insert font-awesome. You can find here: https://fortawesome.github.io/Font-Awesome/cheatsheet/'
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border radius",'events'),
	       "param_name" => "border_radius",
	       "value" => "",
	       "description" => 'For example: 30px;'
	    ),
    	array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border color",'events'),
	       "param_name" => "border_color",
	       "value" => ""
	    ),
	    array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border color hover",'events'),
	       "param_name" => "border_color_hover",
	       "value" => ""
	    ),
	    
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Margin (px)",'events'),
	       "param_name" => "margin",
	       "value" => "",
	       "description" => 'For example: 10px 10px 10px 10px; = (top right bellow left)'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Display animation",'events'),
	       "param_name" => "display_animation",
	       "value" => array(
	       		__('True', 'events') => "true",
	       		__('False', 'events') => "false",
	       	),
	       "default"	=> "true"
	    ),
	    
	    array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => "",
	       "description" => 'Insert class to use for your style'
	    )

	 
)));

/* Countdown */
vc_map( array(
	 "name" => __("Countdown", 'events'),
	 "base" => "events_countdown",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
	  
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("End day",'events'),
		       "param_name" => "end_day",
		       "value" => "",
		       "description" => __('For example: 10','events')

		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("End Month",'events'),
		       "param_name" => "end_month",
		       "value" => "",
		       "description" => __('Insert from 1 to 12. For example: 5','events')

		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("End Year",'events'),
		       "param_name" => "end_year",
		       "value" => "",
		       "description" => __('For example: 2015','events')

		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Display format",'events'),
		       "param_name" => "display_format",
		       "value" => "dHMS",
		       "description" => __("Display Format: dHMS. d: Day, H: Hour, M: Month, S: Second. You can insert HMS or dHM or dH. default dHMS",'events')          
		  ),
		  
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Time zone",'events'),
		       "param_name" => "timezone",
		       "value" => "0",
		       "description" => __('The timezone (hours or minutes from GMT) for the target times. <br/>
					For example:<br/>
					If Timezone is UTC-9:00 you have to insert -9 <br/>
					If Timezone is UTC-9:30, you have to insert -9*60+30=-570. <br/>
					Read about UTC Time: http://en.wikipedia.org/wiki/List_of_UTC_time_offsets','events') 
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label Years",'events'),
		       "param_name" => "years",
		       "value" => "years"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label months",'events'),
		       "param_name" => "months",
		       "value" => "months"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label weeks",'events'),
		       "param_name" => "weeks",
		       "value" => "weeks"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label days",'events'),
		       "param_name" => "days",
		       "value" => "days"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label hours",'events'),
		       "param_name" => "hours",
		       "value" => "hours"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label minutes",'events'),
		       "param_name" => "minutes",
		       "value" => "minutes"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label seconds",'events'),
		       "param_name" => "seconds",
		       "value" => "seconds"
		  ),

		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label Year",'events'),
		       "param_name" => "year",
		       "value" => "year"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label month",'events'),
		       "param_name" => "month",
		       "value" => "month"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label week",'events'),
		       "param_name" => "week",
		       "value" => "week"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label day",'events'),
		       "param_name" => "day",
		       "value" => "day"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label hour",'events'),
		       "param_name" => "hour",
		       "value" => "hour"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label minute",'events'),
		       "param_name" => "minute",
		       "value" => "minute"
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("label second",'events'),
		       "param_name" => "second",
		       "value" => "second"
		  ),
		  array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Class",'events'),
		       "param_name" => "class",
		       "value" => "",
		       "description" => 'Insert class to use for your style'
		  )

	 
)));


/* Register form */
vc_map( array(
	 "name" => __("Register form", 'events'),
	 "base" => "events_registerform",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
	  
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Type",'events'),
				"param_name" => "type",
				"value" => array(
						__('Free', 'events') => "free",
						__('Paypal', 'events') => "pay",
						
					),
				"default"	=> "free"
			),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Title",'events'),
		       "param_name" => "title",
		       "value" => "Register Now",
		  ),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Sub title",'events'),
		       "param_name" => "subtitle",
		       "value" => "* We process using a 100% secure gateway"

		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Style",'events'),
				"param_name" => "style",
				"value" => array(
						__('Style 1', 'events') => "style1",
						__('Style 2', 'events') => "style2",
						
					),
				"default"	=> "style1"
			),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Button text",'events'),
		       "param_name" => "buttontext",
		       "value" => "Register Now"
		  ),
		  
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("button icon. insert font-awesome",'events'),
		       "param_name" => "iconbutton",
		       "value" => "fa-arrow-circle-o-right"
		  ),
		  array(
		       "type" => "colorpicker",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("button background color",'events'),
		       "param_name" => "bg_button",
		       "value" => "#f74949"
		  ),
		  array(
		       "type" => "colorpicker",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("button hover background color",'events'),
		       "param_name" => "bg_button_hover",
		       "value" => "#ffffff"
		  ),
		  array(
		       "type" => "colorpicker",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("text button color",'events'),
		       "param_name" => "text_button_color",
		       "value" => "#ffffff"
		  ),
		  array(
		       "type" => "colorpicker",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("text button hover color",'events'),
		       "param_name" => "text_button_color_hover",
		       "value" => "#f74949"
		  ),
		  array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Class",'events'),
		       "param_name" => "class",
		       "value" => "",
		       "description" => 'Insert class to use for your style'
		)

	 
)));



/* Register form */
vc_map( array(
	 "name" => __("Heading", 'events'),
	 "base" => "events_heading",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
		  array(
		       "type" => "textarea_html",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Content",'events'),
		       "param_name" => "content",
		       "description" => esc_html__("use H2 tag for Title and H3 for subtitle. For example: Click 'text' tab and insert: <h2>About <span style=\"color: #f74949;\">conference</span></h2>
<h3>Whats going on there come and learn</h3>",'events'),
		       "value" => ""
		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display",'events'),
				"param_name" => "style",
				"value" => array(
						__('Left', 'events') => "text-left",
						__('Right', 'events') => "text-right",
						__('Center', 'events') => "text-center",
						
					),
				"default"	=> "text-left"
			),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display border",'events'),
				"param_name" => "hr",
				"value" => array(
						__('Yes', 'events') => "yes",
						__('No', 'events') => "no",
					),
				"default"	=> "yes"
			),
		  array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Class",'events'),
		       "param_name" => "class",
		       "value" => "",
		       "description" => 'Insert class to use for your style'
		  )
	 
)));


/* Schedule */
vc_map( array(
	 "name" => __("Schedule", 'events'),
	 "base" => "events_schedule",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Insert Parent Category (category level 1)",'events'),
		       "param_name" => "array_slug",
		       "description"	=> esc_html__('Go to Schedule >> Categories >> copy value in slug filed of category level 1. For example: sep-26-2015,sep-27-2015,sep-28-2015,sep-29-2015','events'),
		       "value" => ""
		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display sub-category (category level 2) order by: ",'events'),
				"param_name" => "order_by_subcat",
				"value" => array(
			            __('ID', 'events') => 'id',
			            __('Count', 'events') => 'count',
			            __('Slug', 'events') => 'name',
			            ),
			    "default" => 'id'
		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display sub-category (category level 2) order: ",'events'),
				"param_name" => "order_subcat",
				"value" => array(
						__('asc', 'events') => 'asc',
						__('desc', 'events') => 'desc',
					),
				"default"	=> 'asc'
		),
		  
		  array(
		       "type" => "textfield",
		       "holder" => "div",
		       "class" => "",
		       "heading" => __("Item count in each sub-category",'events'),
		       "param_name" => "schedule_count",
		       "value" => "50"
		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display item list order by: ",'events'),
				"param_name" => "order_by_item",
				"value" => array(
			            __('id', 'events') => 'id',
			            __('slug', 'events') => 'name',
			            __('date', 'events') => 'date',
			            __('modified', 'events') => 'modified',
			            __('rand', 'events') => 'rand',
			            ),
			    "default" => 'id'
		  ),
		  array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Display item list order: ",'events'),
				"param_name" => "order_item",
				"value" => array(
						__('asc', 'events') => "asc",
						__('desc', 'events') => "desc",
					),
				"default"	=> "asc"
		),
		
		array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Disable link of title",'events'),
				"param_name" => "turnofflink",
				"value" => array(
						__('No', 'events') => "no",
						__('Yes', 'events') => "yes",
						
					),
				"default"	=> "no"
		),
		array(
		   "type" => "colorpicker",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Time color",'events'),
		   "param_name" => "time_color",
		   "value" => "#44cb9a"
		),
		array(
		   "type" => "colorpicker",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Intermediate color",'events'),
		   "param_name" => "intermediate_color",
		   "value" => "#fac42b"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Class",'events'),
		   "param_name" => "class",
		   "value" => "",
		   "description" => 'Insert class to use for your style'
		)
	 
)));
/* /Schedule */


/* Quickinfo */
vc_map( array(
	 "name" => __("Quickinfo", 'events'),
	 "base" => "events_quickinfo",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Font awesome",'events'),
	       "param_name" => "fonts_icon",
	       "value" => "",
	       "description" => 'Insert font-awesome. For example: fa-heart. You can find here: https://fortawesome.github.io/Font-Awesome/cheatsheet/'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Direction",'events'),
	       "param_name" => "direction",
	       "value" => array(
	       		__('Left', 'events') => "text-left",
	       		__('Right', 'events') => "text-right",
	       	),
	       "default"	=> "text-left"
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title",'events'),
	       "param_name" => "title",
	       "value" => ""
	    ),
	    array(
	       "type" => "textarea",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Description",'events'),
	       "param_name" => "description",
	       "value" => ""
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Time",'events'),
	       "param_name" => "time",
	       "value" => ""
	    ),
	    array(
		   "type" => "colorpicker",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Background color",'events'),
		   "param_name" => "bg_color",
		   "value" => "#000000"
		),
	    array(
		   "type" => "colorpicker",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Text color",'events'),
		   "param_name" => "Color",
		   "value" => "#ffffff"
		),
	    array(
	       "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border Left",'events'),
	       "param_name" => "show_border_left",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	    ),
	    array(
	       "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border Right",'events'),
	       "param_name" => "show_border_right",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )

	 //show_border_right
)));
/* /Quickinfo */

/*events_address vc_map*/
vc_map( array(
	 "name" => __("Address", 'events'),
	 "base" => "events_address",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Font awesome",'events'),
	       "param_name" => "fonts_icon",
	       "value" => "",
	       "description" => 'Insert font-awesome. For example: fa-heart. You can find here: https://fortawesome.github.io/Font-Awesome/cheatsheet/'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Direction",'events'),
	       "param_name" => "direction",
	       "value" => array(
	       		__('Left', 'events') => "text-left",
	       		__('Right', 'events') => "text-right",
	       	),
	       "default"	=> "text-left"
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title",'events'),
	       "param_name" => "title",
	       "value" => ""
	    ),array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title color",'events'),
	       "param_name" => "title_color",
	    ),array(
	       "type" => "textarea",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Description",'events'),
	       "param_name" => "description",
	       "value" => ""
	    ),array(
	       "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Border Right",'events'),
	       "param_name" => "show_border_right",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	    ),
	    array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )

	 //show_border_right
)));
/*end events*/

/*Events topic*/
vc_map( array(
	 "name" => __("Topics", 'events'),
	 "base" => "events_topics_covered",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Font awesome",'events'),
	       "param_name" => "fonts_icon",
	       "value" => "",
	       "description" => 'For example: fa-heart. Insert font-awesome. You can find here: https://fortawesome.github.io/Font-Awesome/cheatsheet/'
	    ),
	    array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Align",'events'),
	       "param_name" => "align",
	       "value" => array(
	       		__('Left', 'events') => "text-left",
	       		__('Right', 'events') => "text-right",
	       		__('Center', 'events') => "text-center",
	       	),
	       "default"	=> "text-left"
	    ),array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Icon color",'events'),
	       "param_name" => "icon_color",
	    )
	    ,array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title",'events'),
	       "param_name" => "title",
	       "value" => ""
	    ),array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title color",'events'),
	       "param_name" => "title_color",
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title Link",'events'),
	       "param_name" => "title_link",
	       "value" => "",
	       "description" => 'For example:http://ovatheme.com'
	    ),array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Target Link",'events'),
	       "param_name" => "target_link",
	       "value" => array(
	       		__('Self', 'events') => "_self",
	       		__('Blank', 'events') => "_blank",
	       		__('Parent', 'events') => "_parent",
	       		__('Top', 'events') => "_top",
	       	),
	       "default"	=> "_self"
	    )
	    ,array(
	       "type" => "textarea",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Info",'events'),
	       "param_name" => "description",
	       "value" => ""
	    ),array(
	       "type" => "textarea_html",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Description",'events'),
	       "param_name" => "content",
	       "value" => "",
	       "description" => __("Open text tab and insert code like: &lt;ul&gt;
	&lt;li&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;a href=&quot;#&quot;&gt;Introduction to WordPress&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;a href=&quot;#&quot;&gt;How WordPress changed the web&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;a href=&quot;#&quot;&gt;Why developers love ?&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;&lt;a href=&quot;#&quot;&gt;Improving WordPress workflow&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;",'events')
	    ),
	    array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )
)));
/*End topic*/




/* Speakers */
vc_map( array(
    "name" => __("Speakers", 'events'),
    "base" => "events_speakers",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "as_parent" => array('only' => 'events_speakers_item', ), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "js_view" => 'VcColumnView',
    "show_settings_on_create" => false,
    "params" => array(
        
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Item count in each slide",'events'),
            "param_name" => "count",
            "value" => "3"
        ),       
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "autoplay",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display pagination",'events'),
            "param_name" => "dots",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));
/* /Speaker */


/*Events speakers item*/

vc_map( array(
	 "name" => __("Speakers item", 'events'),
	 "base" => "events_speakers_item",
	 "as_child" => array('only' => 'events_speakers'),
     "content_element" => true,
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "attach_image",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Thumbnail",'events'),
	       "param_name" => "thumb_image",
	       "value" => "",
	       "description" =>  __("Insert path of thumbnail",'events')
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title",'events'),
	       "param_name" => "title",
	       "value" => ""
	    ),array(
	       "type" => "colorpicker",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title color",'events'),
	       "param_name" => "title_color",
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title Link",'events'),
	       "param_name" => "title_link",
	       "value" => "",
	       "description" => 'For example:http://ovatheme.com'
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Job",'events'),
	       "param_name" => "job",
	       "value" => ""
	    ),array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Target Link",'events'),
	       "param_name" => "target_link",
	       "value" => array(
	       		__('Self', 'events') => "_self",
	       		__('Blank', 'events') => "_blank",
	       		__('Parent', 'events') => "_parent",
	       		__('Top', 'events') => "_top",
	       	),
	       "default"	=> "_self"
	    ),
	    array(
	       "type" => "textarea_html",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("social icon",'events'),
	       "param_name" => "content",
	       "value" => "",
	       "description" => __("click text tab and insert code like:<br/> &lt;ul&gt;
	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;&lt;span class=&quot;hidden&quot;&gt;fb&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;&lt;span class=&quot;hidden&quot;&gt;twitter&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-youtube&quot;&gt;&lt;/i&gt;&lt;span class=&quot;hidden&quot;&gt;youtube&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;",'events')
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )
)));


 if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_events_speakers extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_events_speakers_item extends WPBakeryShortCode {
    }
}

/* /Speakers item */


/*End speakers*/






/* Speakers */
vc_map( array(
    "name" => __("Twitter status", 'events'),
    "base" => "events_twitter_status",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "as_parent" => array('only' => 'events_twitter_status_item', ), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "js_view" => 'VcColumnView',
    "show_settings_on_create" => false,
    "params" => array(
        
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Item count in each slide",'events'),
            "param_name" => "count",
            "value" => "3"
        ),       
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "autoplay",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display pagination",'events'),
            "param_name" => "dots",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));
/* /events_twitter_status */


/*Events events_twitter_status item*/

vc_map( array(
	 "name" => __("Twitter status item", 'events'),
	 "base" => "events_twitter_status_item",
	 "as_child" => array('only' => 'events_twitter_status'),
     "content_element" => true,
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Link of status",'events'),
	       "param_name" => "link",
	       "value" => "",
	       "description" =>  __("Copy link in address taskbar of twitter",'events')
	    )
)));


 if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_events_twitter_status extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_events_twitter_status_item extends WPBakeryShortCode {
    }
}

/* /events_twitter_status item */


/*End events_twitter_status*/



/* background slide */
vc_map( array(
    "name" => __("Background slider", 'events'),
    "base" => "events_bgslide",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "as_parent" => array('only' => 'events_bgslide_item', ), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "js_view" => 'VcColumnView',
    "show_settings_on_create" => false,
    "params" => array(
        
            
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "auto_slider",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display navigation",'events'),
            "param_name" => "navigation",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));
/* /background slide */


/* background slide item*/

vc_map( array(
	 "name" => __("Background Slide item", 'events'),
	 "base" => "events_bgslide_item",
	 "as_child" => array('only' => 'events_bgslide'),
     "content_element" => true,
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "attach_image",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Thumbnail",'events'),
	       "param_name" => "thumb_image",
	       "value" => "",
	       "description" =>  __("Insert path of thumbnail",'events')
	    ),
	    array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )
)));


 if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_events_bgslide extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_events_bgslide_item extends WPBakeryShortCode {
    }
}

/* /background slide item*/









/*Events Nearby Accomodation*/
vc_map( array(
 "name" => __("Nearby Accomodation", 'events'),
 "base" => "events_nearby_accomodation",
 "class" => "",
 "category" => __("My shortcode", 'events'),
 "icon" => "icon-qk",   
 "params" => array(
	array(
	   "type" => "attach_image",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Thumbnail",'events'),
	   "param_name" => "thumbnail",
	   "value" => "",
	   "description" =>  __("Insert path of thumbnail",'events')
    ),
    array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Price",'events'),
	   "param_name" => "price",
	   "value" => ""
	),
	array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Title",'events'),
	   "param_name" => "title",
	   "value" => ""
	),
	array(
	   "type" => "colorpicker",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Title color",'events'),
	   "param_name" => "title_color",
	),array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Title Link",'events'),
	   "param_name" => "title_link",
	   "value" => "",
	   "description" => 'For example:http://ovatheme.com'
	),array(
	   "type" => "dropdown",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Target Link",'events'),
	   "param_name" => "target_link",
	   "value" => array(
	   		__('Self', 'events') => "_self",
	   		__('Blank', 'events') => "_blank",
	   		__('Parent', 'events') => "_parent",
	   		__('Top', 'events') => "_top",
	   	),
	   "default"	=> "_self"
	)
	,array(
	   "type" => "textarea",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Description",'events'),
	   "param_name" => "description",
	   "value" => ""
	)
	,array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("name readmore",'events'),
	   "param_name" => "readmore",
	   "value" => ""
	),
	array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Animation",'events'),
	   "param_name" => "animation",
	   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
	   "value" => "fadeInUp"
	),
	array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
	   "param_name" => "animation_delay",
	   "description" => __("you can insert 0 to remove animation",'events'),
	   "value" => "300"
	),
	array(
	   "type" => "colorpicker",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("readmore color",'events'),
	   "param_name" => "readmore_color",
	   "value" => ""
	)
 )
));  

/*End Events Nearby Accomodation*/

/*Events From our blog*/
$args = array(
  'orderby' => 'name',
  'order' => 'ASC'
  );

$categories=get_categories($args);
$cate_array = array();$arrayCateAll = array('All categories ' => 'all' );
if ($categories) {
	foreach ( $categories as $cate ) {
		$cate_array[$cate->cat_name] = $cate->cat_ID;
	}
} else {
	$cate_array["No content Category found"] = 0;
}
vc_map( array(
	 "name" => __("From Our Blog", 'events'),
	 "base" => "events_from_our_blog",
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
	 	array(
	       "type" => "dropdown",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Category",'events'),
	       "param_name" => "category",
	       "value" => array_merge($arrayCateAll,$cate_array),
	       "description" => __("Choose a Content Category from the drop down list.", 'events')
	    ),array(
    	   "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Total item show",'events'),
	       "param_name" => "total_count",
	       "value" => "20",
	       "description" => __('For example: 10','events')
	     ),
	    array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Columns show",'events'),
			"param_name" => "cols_count",
			"value" => array(
					__('1 Columns', 'events') => "1",
					__('2 Columns', 'events') => "2",
					__('3 Columns', 'events') => "3",
					__('4 Columns', 'events') => "4",
					__('5 Columns', 'events') => "5",
					__('6 Columns', 'events') => "6"
					
				),
			"default"	=> "3"
		),array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show image thumbnail",'events'),
	       "param_name" => "show_thumb",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show title",'events'),
	       "param_name" => "show_title",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show description",'events'),
	       "param_name" => "show_desc",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),array(
    	   "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Text readmore",'events'),
	       "param_name" => "name_readmore",
	       "value" => "",
	       "description" => __('For example: Read more','events')
	     ),array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show read more",'events'),
	       "param_name" => "show_readmore",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show author",'events'),
	       "param_name" => "show_author",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),
	     array(
    	   "type" => "checkbox",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Show date create",'events'),
	       "param_name" => "show_create_date",
	       'value' => array( __( 'Yes', 'js_composerp' ) => true ),
	     ),
	     array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "autoplay",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display pagination",'events'),
            "param_name" => "dots",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
)));
/*End Events From our blog*/


/* Price */

vc_map( array(
 "name" => __("Price", 'events'),
 "base" => "events_pricing",
 "class" => "",
 "category" => __("My shortcode", 'events'),
 "icon" => "icon-qk",   
 "params" => array(
  array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Name",'events'),
       "param_name" => "name",
       "value" => "",
       "description" => 'Name of package. For instance: Personal'
    ),

  array(
       "type" => "dropdown",
       "holder" => "div",
       "class" => "",
       "heading" => __("Price style",'events'),
       "param_name" => "pricing_style",
       "value" => array(   
              __('Currency - Value', 'events') => 'ca',                
              __('Price - Value', 'events') => 'ac',
              ),
       "default" => 'ca'
    ),
  array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Value",'events'),
       "param_name" => "value",
       "value" => "",
       "description" => 'Value of package. For instance: 111'
    ),
  array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Currency",'events'),
       "param_name" => "currency",
       "value" => "",
       "description" => 'Currency of package. For instance: $'
    ),
  array(
       "type" => "colorpicker",
       "holder" => "div",
       "class" => "",
       "heading" => __("Main color",'events'),
       "param_name" => "color",
       "value" => ""
    ),
  array(
       "type" => "dropdown",
       "holder" => "div",
       "class" => "",
       "heading" => __("Feature Package",'events'),
       "param_name" => "feature",
       "value" => array(   
              __('Normal', 'events') => 'nofeature',                
              __('Feature', 'events') => 'featured',
              ),
       "description" => 'Choose package is feature',
       "default" => "nofeature"
    ),
  array(
       "type" => "textarea_html",
       "holder" => "div",
       "class" => "",
       "heading" => __("Content",'events'),
       "param_name" => "content",
       "value" => '',
       "description" => 'Insert Content and button shortcode <br/>[events_button name="REGISTER NOW" link="#register" target="scroll" bg="#f74949" bg_hover="#fff" text_color="#fff" text_color_hover="#f74949" icon="fa-arrow-circle-o-right" border_radius="4px" border_color="#f74949" border_color_hover="#ffffff" padding="11px 20px" margin="2px 5px" animation_delay="0" class="" /]'
   ),
    array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Animation",'events'),
	   "param_name" => "animation",
	   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
	   "value" => "fadeInUp"
	),
	array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
	   "param_name" => "animation_delay",
	   "description" => __("you can insert 0 to remove animation",'events'),
	   "value" => "300"
	),
    array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Class",'events'),
       "param_name" => "class",
       "value" => "",
       "description" => 'Insert class'
    ),

 )));
/* /Pricing */

/* Testimonial */
vc_map( array(
    "name" => __("Testimonial", 'events'),
    "base" => "events_testimonial",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "as_parent" => array('only' => 'events_testimonial_item', ), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "js_view" => 'VcColumnView',
    "show_settings_on_create" => false,
    "params" => array(
               
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "autoplay",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display pagination",'events'),
            "param_name" => "dots",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));
/* /Testimonial */

/* Testimonial item */
vc_map( array(
    "name" => __("Testimonial item", 'events'),
    "base" => "events_testimonial_item",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "content_element" => true,
    "as_child" => array('only' => 'events_testimonial'),
    "params" => array(
        array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __("image",'events'),
            "param_name" => "image",
            "value" => ""
        ),          
        array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __("Description",'events'),
            "param_name" => "description",
            "value" => ""
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Author",'events'),
            "param_name" => "author",
            "value" => ""
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Sub title. Only use for style2",'events'),
            "param_name" => "subtitle",
            "value" => ""
        ),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));

 if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_events_testimonial extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_events_testimonial_item extends WPBakeryShortCode {
    }
}

/* /Testimonial item */


/* Gmap */

vc_map( array(
	"name" => __("Google Map", 'events'),
	"base" => "events_map",
	"class" => "",
	"category" => __("My shortcode", 'events'),
	"icon" => "icon-qk",
	"params" => array(
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Id for map section",'events'),
		   "param_name" => "idmap",
		   "value" => "map-canvas",
		   "description" => 'Insert id to display map. For example: map-canvas'
		),
		array(
		   "type" => "textarea_raw_html",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("location",'events'),
		   "param_name" => "location",
		   "value" => "",
		   "description" => 'Insert latitude parameter for google map. <br/>For example: 51.503454,-0.119562 | 51.499633,-0.124755'
                        
		),
		array(
		   "type" => "textarea_raw_html",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("title",'events'),
		   "param_name" => "title",
		   "value" => "",
		   "description" => 'Insert title parameter for google map. <br/>For example: Hotel 1 | Hotel 2'
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Zoom",'events'),
		   "param_name" => "zoom",
		   "value" => "15",
		   "description" => 'Insert zoom parameter for google map. Default 12'
		),
		array(
		   "type" => "attach_image",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Icon for marker",'events'),
		   "param_name" => "icon",
		   "value" => ""
		), 
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Class",'events'),
		   "param_name" => "class",
		   "value" => "",
		   "description" => 'Insert class'
		),

)));
/* /Gmap */








/* Speakers */
vc_map( array(
    "name" => __("Sponsor", 'events'),
    "base" => "events_sponsor",
    "class" => "",
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",
    "as_parent" => array('only' => 'events_sponsor_item', ), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "js_view" => 'VcColumnView',
    "show_settings_on_create" => false,
    "params" => array(
        
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Item count in each slide",'events'),
            "param_name" => "count",
            "value" => "3"
        ),       
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Duration ms. 1000ms=3s",'events'),
            "param_name" => "duration",
            "value" => "3000"
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Auto play",'events'),
            "param_name" => "autoplay",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Display pagination",'events'),
            "param_name" => "dots",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __("Loop",'events'),
            "param_name" => "loop",
            "value" => array(
                  __('true', 'events') => 'true',
                  __('false', 'events') => 'false',
            ),
            "default"  => 'true'
        ),
        array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
        array(
             "type" => "textfield",
             "holder" => "div",
             "class" => "",
             "heading" => __("Class",'events'),
             "param_name" => "class",
             "default"  => ''
        ),

       
)));
/* /events_sponsor */


/*Events sponsor item*/

vc_map( array(
	 "name" => __("Sponsor item", 'events'),
	 "base" => "events_sponsor_item",
	 "as_child" => array('only' => 'events_sponsor'),
     "content_element" => true,
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(

	 	array(
	       "type" => "attach_image",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Thumbnail",'events'),
	       "param_name" => "thumb_image",
	       "value" => "",
	       "description" =>  __("Insert path of thumbnail",'events')
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Title",'events'),
	       "param_name" => "title",
	       "value" => ""
	    ),array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Alt",'events'),
	       "param_name" => "alt",
	    )
)));


 if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_events_sponsor extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_events_sponsor_item extends WPBakeryShortCode {
    }
}

/* /events_sponsor_item */


/*End events_sponsor*/

/* Tiwtter */
vc_map( array(
    "name" => __("Twitter", 'events'),
    "base" => "events_twitter",
    "content_element" => true,
    "category" => __("My shortcode", 'events'),
    "icon" => "icon-qk",   
    "params" => array( 

        array(
             "type" => "textfield",
             "param_name" => "twitteruser",
             "holder" => "div",
             "class" => "",
             "heading" => __("Twitter User",'events'),
             "value" => "ovatheme",
        ),
        array(
             "type" => "textfield",
             "param_name" => "consumerkey",
             "holder" => "div",
             "class" => "",
             "heading" => __("Consumer key",'events'),
             "value" => "tMkTTjTTUlc21SpRjbekGXzak",
        ),
        array(
             "type" => "textfield",
             "param_name" => "consumersecret",
             "holder" => "div",
             "class" => "",
             "heading" => __("Consumer key secret",'events'),
             "value" => "jLBIgMJb8D6psnqlD2mxfCqcD44I5U9RGAs2Bf6JsQB8lRCFLx",
        ),
        array(
             "type" => "textfield",
             "param_name" => "accesstoken",
             "holder" => "div",
             "class" => "",
             "heading" => __("Access token",'events'),
             "value" => "2444841276-KuW7FFJuTMijF4AoWgdHKwO6oKvx2Ym1wB490E3",
        ),
        array(
             "type" => "textfield",
             "param_name" => "accesstokensecret",
             "holder" => "div",
             "class" => "",
             "heading" => __("Access token secret",'events'),
             "value" => "pvNMa8hEMph0jZjQUT4IRUVMi5yHFO7qMpZ5XJ3eFEqeP",
        ),
        array(
             "type" => "textfield",
             "param_name" => "count",
             "holder" => "div",
             "class" => "",
             "heading" => __("Count",'events'),
             "value" => "3",
        ),
        array(
             "type" => "textfield",
             "param_name" => "pagination",
             "holder" => "div",
             "class" => "",
             "heading" => __("Pagination",'events'),
             "value" => array(
              __('True', 'events') => 'true',
              __('False', 'events') => 'false',
              ),
             "default"  => 'true'
        ),
        array(
             "type" => "textfield",
             "param_name" => "slidespeed",
             "holder" => "div",
             "class" => "",
             "heading" => __("Slide Speed",'events'),
             "value" => "3000",
        ),
        array(
             "type" => "dropdown",
             "param_name" => "autoplay",
             "holder" => "div",
             "class" => "",
             "heading" => __("Autoplay",'events'),
             "value" => array(
            __('True', 'events') => 'true',
            __('False', 'events') => 'false'
            ),
             "default" => "true",
        ),
    	array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Animation",'events'),
		   "param_name" => "animation",
		   "description" => __("You can find effect here: https://daneden.github.io/animate.css/",'events'),
		   "value" => "fadeInUp"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("animation delay(ms). 1000ms = 1s",'events'),
		   "param_name" => "animation_delay",
		   "description" => __("you can insert 0 to remove animation",'events'),
		   "value" => "300"
		),
		array(
		   "type" => "textfield",
		   "holder" => "div",
		   "class" => "",
		   "heading" => __("Class",'events'),
		   "param_name" => "class",
		   "value" => ""
		),
          
    
)));
/* /Tiwtter */

/* Social icon */
vc_map( array(
	 "name" => __("Social icons", 'events'),
	 "base" => "events_social",
	 "as_parent" => array('only' => 'events_social_item'),
	 "js_view" => 'VcColumnView',
     "content_element" => true,
	 "class" => "",
	 "category" => __("My shortcode", 'events'),
	 "icon" => "icon-qk",   
	 "params" => array(
	 	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Class",'events'),
	       "param_name" => "class",
	       "value" => ""
	    )
)));


/* EventBrite */
vc_map( array(
 "name" => __("Iframe Eventbrite", 'events'),
 "base" => "events_iframe_eventbrite",
 "class" => "",
 "category" => __("My shortcode", 'events'),
 "icon" => "icon-qk",   
 "params" => array(
 	
 	
	array(
	       "type" => "textfield",
	       "holder" => "div",
	       "class" => "",
	       "heading" => __("Insert ID of event at eventbrite.com",'events'),
	       "description" => "Find ID. This is your event url: https://www.eventbrite.com/e/sell-imevent-wordpress-theme-tickets-19209099935 => ID is 19209099935",
	       "param_name" => "id",
	       "value" => "",               
	  ),
  	array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Class",'events'),
       "param_name" => "class",
       "value" => ""
    ),

)));



vc_map( array(
 "name" => __("Social icon item", 'events'),
 "base" => "events_social_item",
 "content_element" => true,
 "as_child" => array('only' => 'events_social'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
 "class" => "",
 "category" => __("My shortcode", 'events'),
 "icon" => "icon-qk",   
 "params" => array(
	array(
       "type" => "textfield",
       "holder" => "div",
       "class" => "",
       "heading" => __("Font awesome",'events'),
       "param_name" => "fonts_icon",
       "value" => "",
       "description" => 'For example: fa-heart. Insert font-awesome. You can find here: https://fortawesome.github.io/Font-Awesome/cheatsheet/'
    ),array(
	   "type" => "colorpicker",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("color fonts icon",'events'),
	   "param_name" => "icon_color",
	),array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("icon Link",'events'),
	   "param_name" => "icon_link",
	   "value" => "",
	   "description" => 'For example:http://ovatheme.com'
	),array(
	   "type" => "dropdown",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("Target Link",'events'),
	   "param_name" => "target_link",
	   "value" => array(
	   		__('Self', 'events') => "_self",
	   		__('Blank', 'events') => "_blank",
	   		__('Parent', 'events') => "_parent",
	   		__('Top', 'events') => "_top",
	   	),
	   "default"	=> "_self"
	),array(
	   "type" => "textfield",
	   "holder" => "div",
	   "class" => "",
	   "heading" => __("class",'events'),
	   "param_name" => "class",
	   "value" => "",
	)
 )
));  

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
  class WPBakeryShortCode_events_social extends WPBakeryShortCodesContainer {
  }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_events_social_item extends WPBakeryShortCode {
  }
}
/* Social icon */


}} /* /if //function */
?>