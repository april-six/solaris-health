=== Gravity Forms Encrypted Fields ===

Contributors: Neil Rowe
Tags: data privacy, data protection, database encryption, encrypt, encrypt database, encrypt field, encrypt form, encrypted fields, encryption, gdpr, gravity, gravity forms, personal data, private data, sensitive data
Requires at least: 4.4
Tested up to: 4.9.8
Stable tag: 4.4.1
License: copyright 2016 Neil Rowe / Plugin Owl





== Description ==

This plugin extends Gravity Forms with options to use database storage encryption or simply hide field values for individual Gravity Forms fields with both global and individual user/role based view permission options. The plugin also includes tools for effective encrypted data management.

Available Field Types: text, textarea, date, name, number, email, phone, website, address, dropdown, radio, multi select, checkbox, list.





= Requirements =

* WordPress 4.6+
* PHP 5.6+  *note:* 5.5 and 5.4 should also function but are NOT SUPPORTED
* Gravity Forms Version 2.0.7+
* Server must support one of the following encryption methods:
	* OpenSSL Encryption Enabled -ver 3.0+
	* Mcrypt Encryption Enabled -required for ver 2.9.3 or previous

It is important to test the plugin for reliable data encryption and decryption within a test site on non-essential data before running in any live production environment with sensitive and/or essential data. 





== Installation ==

**Always backup your WordPress database and files before installing any plugins or updates!**

**You will need your Envato Purchase Code to register the plugin license.**

You can install Gravity Forms Encrypted Fields through the "Add Plugins" page of the "Plugins" menu in WordPress. Plugins => Add New => Upload Plugin. Upload the plugin file without unzipping it. Or follow directions below for FTP install.

1. Unzip the plugin file.
2. Upload with your FTP software the "gravity-forms-encrypted-fields" folder, and only this folder, to your plugins directory. It should be "wp-content/plugins".
3. Activate the plugin through the "Plugins" menu in WordPress.
4. Follow setup instructions to configure the plugin through the "Settings->GF Encrypted Fields" options page in WordPress.





== Upgrading the plugin ==

**You will need your Envato Purchase Code to register the plugin license if it is not already registered from a previous version.**

IMPORTANT: If upgrading from a version prior to 3.0, before installing 3.0 you must first use the encrypt/decrypt tool (see below for quick instructions) to decrypt ALL previous entries with encryption and then upgrade to ver 3.0 following below instructions and then select encryption type on settings options page and save changes, then encrypt ALL previous entries again to resume functionality.
Previously encrypted entries in prior versions CANNOT be decrypted/read using new encryption methods in ver 3.0+.

To use the settings page "Encrypt/Decrypt Form Entries" tool you should back up your Wordpress installation and database and read and follow the tools instructions first. Then you only have to specify "decrypt" , enter a form ID, make sure "ENCRYPT ASSIGNING USER OWNED FIELDS" is on,  and enter 200 for max entries per run. 
Save changes, and then if you have more than 200 entries for that form specify "decrypt" again and set the "OFFSET RUN START POINT" to 200 (where first run left off) and run again. 
Repeat for each form and change the "OFFSET RUN START POINT" to where last run left off in increments of 200 until complete.
Once upgraded to new version, repeat this process using "encrypt" to re-encrypt the entries with new encryption.

**Please use the [Envato Market](https://envato.com/market-plugin/) plugin to upgrade the plugin for versions 4.8+. Remember to re-enter your custom website key if you are using one immediatly after updating. Custom website keys updated in ver 4.3.9+ attempt to auto regenerate themselves but should still be checked on the settings page after upgrade/reinstallation. If you must upgrade manually follow the instructon below.**

1. Backup your database and WordPress installation, and copy your sites current Encryption Password and Website Key to a safe location. The Website Key will have to be regenerated on update or removal and reinstallation and may possibly change depending on configuration file changes you have made to your WordPress install. You can change it back with your copy if needed. If you want to keep your settings page password active without the need to re-enter it, please check the box by it for 'No password removal on plugin removal and reinstall'. You may wish to change this back after the reinstall.
2. Save an offline backup copy of the current versions gravity-forms-encrypted-fields plugin folder located in your /wp-content/plugins directory. 
3. Offline your sites front end and deactivate the "Gravity Forms Encrypted Fields" plugin. Unzip the installation file and use FTP to replace your sites gravity-forms-encrypted-fields folder contents. Do NOT replace the "includes" folder or you will have to regenerate your website key or enter in your custom one again. The folder contents are normally located in /wp-content/plugins/gravity-forms-encrypted-fields. 
If you can not use FTP to upgrade you can deactivate and uninstall the old version and install the new one through the WordPress plugin page installer. If you are using this method you should immediately reenter any custom website key used after updating. Custom website keys updated in ver 4.3.9+ attempt to auto regenerate themselves but should still be checked on the settings page after upgrade/reinstallation. You will also need to reenter your settings page password if you are using one unless you have it selected to not be removed on plugin uninstallation. You should also double check that the "Delete Plugin Options on Removal" setting is not selected so that your settings will be saved on plugin removal.
4. Re-activate the "Gravity Forms Encrypted Fields" plugin. Go to the "Settings->GF Encrypted Fields" options page to register the plugin license with your Envato Purchase Code if not already done from a previous version and then save settings to regenerate your auto website key if using a version prior to 4.8. If the site key was changed since the autogenerated one, replace this with the one you copied in step 1 and save settings.
5. If upgrading from version < 3.0 to version 3.0+ YOU MUST SELECT YOUR ENCRYPTION TYPE ON SETTINGS SCREEN ON UPGRADE TO RESUME ENCRYPTION FUNCTIONALITY.
6. If you encounter errors after upgrading, please revert back to the earlier version you saved a backup copy of until any version errors are corrected.
7. Check the Changelog below and be sure to read the current versions additions/changes and make any neccessary configuration adjustments then take the site out of maintenance mode.





== Upgrade Notice ==

UPGRADE NOTICE
**Important: You will need your Envato Purchase Code for the plugin to register the plugin license after upgrading if it is not already registered.**
* Always be sure to backup your website and database before upgrading your plugins and safely store a copy of this plugins Encryption Password and Website Key.
* If you are using a custom website key you need to be sure to either re-enter the custom website key on the plugin settings page IMMEDIATELY after any plugin update or reinstallation, or follow the manual plugin update instruction in the plugins readme.txt file to preserve the custom website key on upgrade/reinstallation. If you are not sure if your website key is custom please check the "Website Key" setting on the plugin settings page after refreshing it. Custom website keys created or updated in ver 4.3.9+ attempt to auto regenerate themselves but should still be checked on the settings page immediately after upgrade/reinstallation.

New in version 4.4.2
* Changed code for signature support in ALL type decrypted merge tags.
* Removed unnecessary custom merge tag javascript from entry, entry_detail, and entry_detail_edit views.
* Disabled Mcrypt encryption option selection if Mcrypt is not supported on server/PHP version.
* Streamlined decryption function for non-encrypted fields.
* Added gfef_text_encrypt($text, $key) and gfef_text_decrypt($text, $key) and documentation for developers to encrypt/decrypt any values they would like. A custom key can be passed.





== Changelog ==

== 4.4.2 ==
* Changed code for signature support in ALL type decrypted merge tags.
* Removed unnecessary custom merge tag javascript from entry, entry_detail, and entry_detail_edit views.
* Disabled Mcrypt encryption option selection if Mcrypt is not supported on server/PHP version.
* Streamlined decryption function for non-encrypted fields.
* Added gfef_text_encrypt($text, $key) and gfef_text_decrypt($text, $key) and documentation for developers to encrypt/decrypt any values they would like. A custom key can be passed.

== 4.4.1 ==
* Added support for signature fields to the ALL type decrypted merge tags.
* Fixed issues with single field and single input (from multi-part-fields) custom merge tag drop downs.
* Removed custom merge tags from the form editor merge tag drop downs.

== 4.4.0 ==
* Added ability to insert any available UNLOCKED decrypted/encrypted merge tags for a form directly from the Gravity Forms merge tag dropdown selector in admin.
* Improved visual formatting of ALL and ALL+ type merge tags.
* Changed "Allow Front End Display" option name to "Admin Area Only Viewing".
* Fixed an issue where no BATCH SIZE specified in admin encrypt/decrypt tool resulted in attempting to process ALL entries for a form.
* Fixed an issue with “Remove User IP Address From Form Submissions” on forms with the “save and continue” option set for the form.
* Fixed list fields not being included in the "Global Form Encryption Switch" setting.
* Updated instructions.

== 4.3.9 ==
* Updated optional removal of users IP from both incomplete(save and continue) and partial entries. 
* Updated custom website keys to automatically regenerate after updates or re-instalation for any custom website keys entered or updated in ver 4.3.9+.
* Updated website key notice to reflect if using custom or auto key without refreshing page.
* Added admin notice to check Website Key after updates/reinstallation.
* Added decryption helper function for developers: Returns decrypted Gravity Forms field value or false, and can optionally attempt to have Gravity Forms HTML format the display. - gfef_developer_decrypt($entry_id, $field_id, $format).
* Fixed entries in trash showing up in "ALL" entries list.
* Fixed undefined offet notice on entries list page.
* Updated instructions.
* Tested on WordPress 4.9.8

== 4.3.8 ==
* Automatic updates and available update notifications with updated version changes viewable through the free "Envato Market" plugin.
* Added "ANY" as a valid FIELD ID entry in the decrypted merge tag options to unlock ALL {gfef_decrypt_FIELD ID} tags for a form with a single unlock entry. This can be combined with "ALL" as a FORM ID to unlock ALL {gfef_decrypt_FIELD ID} tags for ALL forms with a single unlock entry. This is also able to be combined with user decrypted merge tags and encrypted merge tags. 
* Added "ALL" as a valid FORM ID entry in the decrypted merge tag options to unlock either the {gfef_decrypt_ALL}, {gfef_decrypt_ALL+}, {gfef_decrypt_ALL_USER}, or {gfef_decrypt_ALL+_USER} tag for ALL forms with a single unlock entry.
* Added "Universal Unlocks" which unlock ALL available decrypted/encrypted merge tags for ALL forms and ANY field.
* Updated, improved, and organized instructions and documentation.
* Added Video tutorials to instructions and documentation.
* Fixed formatting of date fields in all decrypted merge tags to match the formatting selected in the form editor for field.
* Fixed issues with native search permission when using roles.
* Fixed decrypted merge tags for date fields not using the date picker UI
* Changed file upload fields printout to html links to the file for ALL and ALL+ decrypted merge tags.
* Removed file upload fields links from core all_fields and plugin ALL and ALL+ merge tags if files are being deleted.
* If using the unique and secure auto generated website key it now regenerates automatically without visiting the settings page after plugin update/removal and reinstall if settings are saved on uninstall. 
* Added notice to Mcrypt encrytion type option for systems on PHP 7.2+.
* Fixed various PHP notices/warnings.
	
== 4.3.7 ==
* Fixed various potential PHP notices.
	
== 4.3.6 ==
* Fixed issue with both remove IP and delete for selected for the same form.

== 4.3.5 ==
* Fixed issue with delayed encryption for non logged in users.
* Improved documentation.

== 4.3.4 ==
* Added "Process Feeds/Add-Ons before encrypting" option which delays encryption until after the data has been initially processed during submission through most feeds and addons.
* Added ability to delete individual field data from specified forms after submission/notifications/etc.
* Fixed some potential PHP notices for certain PHP versions when running in debug mode.

== 4.3.21 ==
* Improvements to settings page instructions and buttons.
* Changed Section Breaks to print only if fields in section have data in all "ALL+" type custom tags.
* Removed Section Break Descriptions from all "ALL+" type custom tags.

== 4.3.2 ==
* Added Section Break labels and descriptions to all “ALL+” type custom tags.
* Changed “Name” fields printout to single line in all “ALL” and "ALL+" type custom tags.
* Changed “Address” fields printout for better address formatting in all “ALL” and "ALL+" type custom tags.
* All “ALL” and "ALL+" type custom tags code rewritten for exponential performance increase.
* Changed all “ALL” and “ALL+” type custom tags to more closely visually replicate standard {all_fields} tag.

== 4.3.1 ==
* Fixed Method callback handling for Gravity Forms versions prior or post 2.3 

== 4.3 ==
* Gravity Forms 2.3rc-5 compatibility.
* Changed addtional core functionality to utilize the GFAPI

== 4.2.3 ==
* Fixed merge tag filtering for list fields.

== 4.2.2 ==
* Fixed masking functionality for multiple masked fields (ver 4.0 - 4.2.1)
* Tested on WordPress 4.9.4

== 4.2.1 ==
* Full compatibility with Gravity Forms Partial Entries Add-On.

== 4.2 ==
* Tested with Gravity Forms 2.3 
* Tested with PHP 7.1.10.
* Fixed various notices in WPDebug mode.

== 4.1.1 ==
* Improved Setup Instructions

== 4.1 ==
* Improved Setup Instructions

== 4.0 ==
* Added list fields as fully supported field type for encryption.
* Added the {gfef_decrypt_ALL+} merge tag which outputs decrypted display of all fields from entry form regardless if data is encrypted or hidden. The behavior of this merge tag closely replicates the standard Gravity Forms {all_fields} tag but it decrypts all data.
* Added the {gfef_decrypt_ALL+_USER} merge tag which outputs decrypted display of all fields the current user has access to from the entry form regardless if data is encrypted or hidden. The behavior of this merge tag closely replicates the standard Gravity Forms {all_fields} tag but allows for data the user has permissions to to be viewed where the normal {all_fields} tag would show a restricted display do to the merge tag filter.
* Added ability to use roles in the User/Role Access List to quickly give access to all users in a role.
* Added listing of role names/slugs to settings that accept roles for permissions.
* Improved decrypted/encrypted merge tag option instructions.
* Fixed Undefined variable notice when running in debug mode.

== 3.9.4 ==
* Improved setup and usage instructions.
* Tested on WordPress 4.9

== 3.9.3 ==
* Added “Remove User IP Address From Form Submissions” setting to remove the users IP address from entry properties on submitted entries (IP is still able to be stored encrypted or unencrypted through standard field when using this option)
* Clarified instructions.
* Added default text for "Encrypted Field Restricted Display" and "Hide Field Value Restricted Display" so users without permission do not see blank restricted values by default on setup.
* Removed browser autocomplete from select inputs on settings page.

== 3.9.2 ==
* Added settings page link to plugins page to help guide new users. 

== 3.9.1 ==
* Updated the fields advanced options display to no longer show the blue dividing lines when options are not present.

== 3.9 ==
* Updated the {gfef_decrypt_ALL} merge tag to output fields in form order instead of field_ID order
* Updated the {gfef_decrypt_ALL} merge tag to output multi-part fields in a singular table row under a single label
* Updated locked out settings page with "unlock" button and notification for incorrect password entry
* Updated Settings Page LOCKOUT Password with a verification field and notifications for invalid or non-matching password entries

== 3.8 ==
* Added per field view permission option "Original Submitting User View Permission", which allows original submitting users view permissions to their own individual entry field data in addition to the normal "User/Role View Permission" list users/roles.
* Subtle visual changes

== 3.7 ==
* Added Hide Field Value controls to the "Global Form Encryption Switch"
* Fixed "Global Form Encryption Switch" to turn off hide field value when encryption turned on and turn off encryption when hide field value turned on.
* IMMEDIATE BUG FIX for 3.6: form fields not rendering

== 3.6 ==
* Added encrypted/hidden indicators by field labels for quick reference of encrypted/hidden fields in the form editor without having to open the field options.
* Added "Global Form Encryption Switch" to turn on/off encryption for all supported fields on a specified form to settings page.
* Updated instructions.

== 3.5 ==
* Improved multi input field labels in {gfef_decrypt_ALL} merge tags.
* Code improvements/bug fix for {gfef_decrypt_ALL} merge tags not rendering all encrypted fields.
* Fixed {gfef_decrypt_ALL} merge tags to also include hidden field output.
* Added {gfef_decrypt_user_FIELD ID} merge tags to allow for specific field output through merge tags that checks the users view permissions to the field for use in "Gravity View" and other use cases where the merge tag is generating site content.
* Added {gfef_decrypt_ALL_USER} merge tag to allow for ALL encrypted and hidden field output through a single merge tag that checks the users view permissions to the fields for use in "Gravity View" and other use cases where the merge tag is generating site content.
* Bug fix for "undefined variable" when using the "encrypt/decrypt" tool.

== 3.4 ==
* Added instructions for manual website key generation for installations on web servers with security restrictions preventing auto generation.
* Code improvements.

== 3.3 ==
* Added plugin version reporting to unlocked settings page.
* Update author links.
* Added explanation for 0 entries processed if no ENTRY IDs are specified and MAX ENTRIES PER RUN is left blank to the encryption/decryption tool report.

== 3.2 ==
* Added {gfef_decrypt_ALL} merge tag for ability to include decrypted output of ALL encrypted and hidden fields thorugh Decrypted Merge Tag tool output.
* Added Encryption Verification Mode Option to encryption test section of setting page to reveal raw encrypted data on entries pages for verification of encryption.
* Adjusted decrypted merge tag tool to no longer allow output of user owned fields unless original logged in submitting owner generates merge tag results.
* Form Encrypt/Decrypt Tool and Settings Page Lockout Password are now hidden by default to prevent accidental entries.
* Cleaned up options page with subtle visual improvements and Guide materials visibility toggles.
* Minor code improvements and CSS style streamlining.
* NOTICE: If upgrading from version prior to 3.0, before installing 3.0 you must first use the encrypt/decrypt tool to decrypt ALL previous entries with encryption and then upgrade to ver 3.0 and select encryption type and save changes, then encrypt previous entries again to resume functionality. Previously encrypted entries in prior versions CANNOT be decrypted/read using new encryption methods in ver 3.0+. IMPORTANT: Please always refer to the plugin’s readme file for detailed instructions on upgrading between versions.

== 3.1
* Expanded Native Search Feature to attempt finding the entered search terms with varying capitalization automatically.
* Fixed Encryption Test to also warn users when encryption password override is on while using Open SSL encryption.
* Clarified Encryption Password Override instructions.
* Subtle visual changes to settings options page for more clear section breaks.
* NOTICE: If upgrading from version prior to 3.0, before installing 3.0 you must first use the encrypt/decrypt tool to decrypt ALL previous entries with encryption and then upgrade to ver 3.0 and select encryption type and save changes, then encrypt previous entries again to resume functionality. Previously encrypted entries in prior versions CANNOT be decrypted/read using new encryption methods in ver 3.0+

== 3.0 ==
* Added OpenSSL encryption with ability to select and switch encryption type. 
* Changed Mcrypt encryption to add additional level of security. 
* Added ability to search user based fields with “native search”.
* Changed "Encrypt/Decrypt Form Entries" tool to ONLY encrypt fields with encryption currently turned ON in form by default.
* Added option to "Encrypt/Decrypt Form Entries" tool to allow for encryption of fields with encryption currently turned OFF in form.
* Added subtle visual improvements to settings page.
* NOTICE: If upgrading from version prior to 3.0, before installing 3.0 you must first use the encrypt/decrypt tool to decrypt ALL previous entries with encryption and then upgrade to ver 3.0 and select encryption type and save changes, then encrypt previous entries again to resume functionality. Previously encrypted entries in prior versions CANNOT be decrypted/read using new encryption methods in ver 3.0+

== 2.9.3 ==
* Added native search functionality of encrypted field data. Does not include user owned fields.
* Added "Search Permission" option to limit users/roles ability to search based on encrypted field data.
* Added Encrypted Merge Tags {gfef_encrypt_FIELD ID} for developers to output an encrypted version of any field data in notifications and confirmations to be decrypted elsewhere by data or email recipient. Data that is unencrypted will still have an encrypted version output by this merge tag.
* Added floating "Save Changes" button to settings options page to assist quicker settings modifications.

== 2.9.1 ==
* Added "Search Data" option to setting screen to allow for searching entry field data based on stored encrypted strings. ..This allows for stable search of encrypted fields.

== 2.9 ==
* Added ability to enter roles to fields user view permissions and the limit user view permissions list. Using role slugs in these locations now controls permissions for all user of a certain role per field without the need to add all the user names under that role. Individual users can be restricted elsewhere such as the lockout list and the finer grain individual user permissions will override the role locking the individual users out.
* Fixed the "Limit User/Role View Permission Lists" functionality so that when users/roles are in here, any field with a blank "User/Role View Permission List" still gives access to ALL users, but only users/roles in the limiting list will be valid if using the fields "User/Role View Permission List" to limit an individual fields view permissions.   !!!!!! Please check setup on update. This will give access where there previously wasn't if a fields "User/Role View Permission List" was blank but was restricting access still !!!!! 

== 2.7 ==
* Added ability to auto delete specified form entries after submission/user registration.
* Added ability to auto delete specified form file uploads after submission/user registration.
* Added ability to attach specified form file uploads to specified notifications after submission/user registration before entry or file uploads are deleted.

== 2.5 ==
* Added custom output preview masking for fields including hidden/encrypted fields to use for entry view, and optionally also in merge tags.
* Added complete permissions bypass for full decrypted output (optionally including merge tags) for user specified form fields.
* Added admin controlled decrypted merge tags to allow for full decrypted output of field data in merge tags within confirmations and notifications while still keeping all website view permissions.
* Changed the Merge Tag Filter to allow for both the filtering of individual field merge tags as well as the "all Fields" merge tag.
* Removed browser autocompletion from settings page lockout password field and settings page. Please update your lockout password, or clear browsers autocomplete cache/storage after upgrading to this version. Nothing is reccommended on new installs.
* Added notice to failed encryption test for when encryption override password is on.

== 2.3 ==
* Added Form, Entry, and Field specific encryption and decryption with user based field control through admin options settings page.
* Added Encryption/Decryption Reporting to admin settings options page to report status of manual encryption/decryption runs.
* Improoved priority of encryption function to allow for other hooked functions to be pre-processed.
* Added encryption to admin option settings page password for database storage.
* Fixed display of 0 value data when encrypted.
* Code cleanup and improvements

== 2.0 ==
* Added 'User Owned Field' advanced field option to allow ONLY the logged in user who originally submitted the data to be able to view it as readable even if another user updates the data. -This overrides ALL other user permissions.
* Added *conditional* option to allow front end display of encrypted/hidden data for users with permission
* Added option to save settings when deleting plugin to install updated version
* Added option to save admin options page password regardless of deleting other settings to prevent password bypass when uninstalling and reinstalling plugin
* Code cleanup and improvements

== 1.7.2 ==
* Added Feature to optionally password protect admin options screen

== 1.7.1 ==
* Bug fix for salt creation and zero value data

== 1.7 ==
* Added Merge Tag Filter and {all_fields} Merge Tag Exclude/Include Options
* Expanded the system check to include encryption bypass and merge tag filter for quick overview of system settings

== 1.6 ==
* Added Feature to 'Limit User View Permission Lists'

== 1.5 ==
* Added Feature 'User Access List'
* Fixed encryption of zero value data.
* Added Instructions and in depth option descriptions

== 1.2 ==
* Added Hide Field data safe option
* Changed decryption to no longer require field encryption turned on for users with permission
* Changed restricted displays to respond to whether or not data is actually encrypted or just hidden

== 1.0 ==
* Initial version