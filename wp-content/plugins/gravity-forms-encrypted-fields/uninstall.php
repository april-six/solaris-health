<?php
// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN') ||	!WP_UNINSTALL_PLUGIN ||	dirname(WP_UNINSTALL_PLUGIN) != dirname(plugin_basename(__FILE__))) {
	status_header(404);
	die;
}

//deregister site
function gfef_plugin_owl_deregister() {
	$pluginowl_license 			= get_option('gfe_pluginowl_licensed');
	$pluginowl_license_check 	= false;
	$pluginowl_license_date		= current_time('Y/m/d H:i:s', 1);
	if (is_array($pluginowl_license)) {
		$pluginowl_license_check 	= isset($pluginowl_license[0]) ? $pluginowl_license[0] : false;
		$pluginowl_license_date		= isset($pluginowl_license[1]) ? $pluginowl_license[1] : $pluginowl_license_date;
	}
	$pluginowl_license_request 	= wp_remote_request('https://www.pluginowl.com/wp-json/pluginowl-licensing/v1/license-manager', array(
		'method' => 'POST',
		'body'   => array(
			'pluginowl_license_API' 		  	=> 'API_REQUEST',
			'pluginowl_license_purchase_code' 	=> $pluginowl_license_check ? $pluginowl_license_check : $pluginowl_license,
			'pluginowl_license_website_name'  	=> get_bloginfo('name'),
			'pluginowl_license_web_address'		=> get_bloginfo('url'),
			'pluginowl_license_activation_date' => $pluginowl_license_date,
			'pluginowl_license_product_id' 		=> '.36eNVchaa0J5b0wb5nwvmjyUVHCX6zOqQP8kw.9GDPSWKMJ/AH8a$01$y2$',
			'pluginowl_remove_license' 			=> true
		)
	));
	delete_option('gfe_pluginowl_licensed');
	delete_option('gfe_pluginowl_licensed_save');
	delete_option('gfe_pluginowl_remove_license');
}
if (is_array(get_option('gfe_pluginowl_licensed'))) {
	gfef_plugin_owl_deregister();
}

//normal options removal
if (get_option('gfe_delete_options')) {
	// for site options
	delete_option('gfe_pluginowl_licensed');
	delete_option('gfe_pluginowl_licensed_save');
	delete_option('gfe_pluginowl_remove_license');
	delete_option('gfe_encryption_method');
	delete_option('gfe_delete_options');
	delete_option('gfe_admin_only');
	delete_option('gfe_encryption_bypass');
	delete_option('gfe_restricted');
	delete_option('gfe_hidevalue');
	delete_option('gfe_custom_data_search');
	delete_option('gfe_show_encryption');
	delete_option('gfe_masking');
	delete_option('gfe_decrypt_merge_tags');
	delete_option('gfe_delete_ip');
	delete_option('gfe_delete_field_data');
	delete_option('gfe_delete_entry');
	delete_option('gfe_delete_file_uploads');
	delete_option('gfe_attach_file_uploads');
	delete_option('gfe_mergefilter');
	delete_option('gfe_user_search_permission_list');
	delete_option('gfe_limit_user_view_permission_list');
	delete_option('gfe_user_lockout_list');
	delete_option('gfe_user_lockout_override_list');
	delete_option('gfe_website_key');
	delete_option('gfe_encryption_key');
	delete_option('gfe_encryption_key_override');
	delete_option('gfe_settings_key');
	delete_option('gfe_global_encryption_on');
	delete_option('gfe_global_encryption_on_off');
	delete_option('gfe_global_encryption_encrypt_hide');
	delete_option('gfe_encrypt_decrypt_form');
	delete_option('gfe_encrypt_decrypt_form_entries');
	delete_option('gfe_encrypt_decrypt_form_fields');
	delete_option('gfe_encrypt_decrypt_form_entry_paging');
	delete_option('gfe_encrypt_decrypt_form_ubf');
	delete_option('gfe_encrypt_decrypt_form_encrypt_all');
	delete_option('gfe_encrypt_decrypt_user');
	delete_option('gfe_encrypt_decrypt');
	
	// for site options in Multisite
	delete_site_option('gfe_pluginowl_licensed');
	delete_site_option('gfe_pluginowl_licensed_save');
	delete_site_option('gfe_pluginowl_remove_license');
	delete_site_option('gfe_encryption_method');
	delete_site_option('gfe_delete_options');
	delete_site_option('gfe_admin_only');
	delete_site_option('gfe_encryption_bypass');
	delete_site_option('gfe_restricted');
	delete_site_option('gfe_hidevalue');
	delete_site_option('gfe_custom_data_search');
	delete_site_option('gfe_show_encryption');
	delete_site_option('gfe_masking');
	delete_site_option('gfe_decrypt_merge_tags');
	delete_site_option('gfe_delete_ip');
	delete_site_option('gfe_delete_field_data');
	delete_site_option('gfe_delete_entry');
	delete_site_option('gfe_delete_file_uploads');
	delete_site_option('gfe_attach_file_uploads');
	delete_site_option('gfe_mergefilter');
	delete_site_option('gfe_user_search_permission_list');
	delete_site_option('gfe_limit_user_view_permission_list');
	delete_site_option('gfe_user_lockout_list');
	delete_site_option('gfe_user_lockout_override_list');
	delete_site_option('gfe_website_key');
	delete_site_option('gfe_encryption_key');
	delete_site_option('gfe_encryption_key_override');
	delete_site_option('gfe_settings_key');
	delete_site_option('gfe_global_encryption_on');
	delete_site_option('gfe_global_encryption_on_off');
	delete_site_option('gfe_global_encryption_encrypt_hide');
	delete_site_option('gfe_encrypt_decrypt_form');
	delete_site_option('gfe_encrypt_decrypt_form_entries');
	delete_site_option('gfe_encrypt_decrypt_form_fields');
	delete_site_option('gfe_encrypt_decrypt_form_entry_paging');
	delete_site_option('gfe_encrypt_decrypt_form_encrypt_all');
	delete_site_option('gfe_encrypt_decrypt_form_ubf');
	delete_site_option('gfe_encrypt_decrypt_user');
	delete_site_option('gfe_encrypt_decrypt');
}

//no options screen password recovery options removal
if (!get_option('gfe_norecover_pass')){
	// for site options
	delete_option('gfe_settings_lock');
	delete_option('gfe_norecover_pass');
	
	// for site options in Multisite
	delete_site_option('gfe_settings_lock');
	delete_site_option('gfe_norecover_pass');
}