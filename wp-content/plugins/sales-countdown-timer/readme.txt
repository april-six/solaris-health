=== Sales Countdown Timer ===
Contributors: villatheme, mrt3vn
Donate link: https://www.villatheme.com/donate
Tags: sale countdown timer woocommerce, sales countdown woocommerce addon, woocommerce countdown, woocommerce sales countdown, woocommerce product countdown, woocommerce sales scheduler, woocommerce sales timer, woocommerce scheduler, woocommerce timer
Requires at least: 4.4
Tested up to: 5.2
Stable tag: 1.0.4.2
License: GPLv2 or later

== Description ==

Sales Countdown Timer helps display countdown timer for scheduled sale products and a progress bar to make your customers feels the urgency and scarcity.

>[Documents](http://docs.villatheme.com/?item=sales-countdown-timer "Documents")

### FEATURES

&#9658; **General**:

- **Message**: You can edit message to show as well as a text above the countdown timer besides a countdown timer running

- **Schedule time**: Set the start and end time for the use of shortcode countdown timer

- **Countdown type**: Display a countdown timer or just show the remaining time

- **Auto switch**: The countdown timer can be automatically switched from just show remaining time to countdown when time left is less than a specific value

- **Multiple settings**: You can set how many countdown timers at a time as you wish

- **Shortcode**: Just click the button copy shortcode, a shortcode to use the selected countdown timer will be copied to clipboard. All you have to do is to paste it wherever your want it to be displayed

&#9658; **Design**:

- **Time separator**: You can set to colon(:), comma(,), dot(.) or blank

- **Datetime format**: Thought it's inconvenient to make you choose it this way but it's basically many enough for you to select and very nice for translate purpose using a translate plugin

- **Display type**: Six type for you to choose, including just showing as a line of text

- **Design**: You can customize color, background color, padding and border radius(curve corner) of the countdown timer; You can change the color, background color and font size of time unit and time value, too. All these design settings are very easy to use with an intuitive live preview

&#9658; **Product sale countdown-most important feature of this plugin**:

- **Schedule time**: On page edit product, you can set schedule time to minutes of day, and select a countdown timer profile(through the countdown timer name) for a product

- **Settings and Design**: The setting and design of a product's sales countdown timer is from a countdown timer that you select on page edit product through the countdown timer name that you created.

- **Position**: Can be set before/after product price, sale flash, add-to-cart button or inside product image

- **Shop page, category page**: Yes you can make the countdown timer show on category or shop page. Each product has it's own countdown timer with it's own sale time as you set for it

- **Upcoming sale**: Not just available for on-sale products, you can use countdown timer for upcoming sale products to make your customers long for it to come. Cool, right.

- **Count bar**: Count bar can show percentage/quantity remaining or claimed. Currently it has only one style but can be customized width, height, color, background color and border radius. More style will be added in the future.

### MAY BE YOU NEED

[Import Shopify to WooCommerce](http://bit.ly/import-shopify-to-woocommerce): Import Shopify to WooCommerce plugin help you import all products from your Shopify store to WooCommerce

[Customer Coupons for WooCommerce](http://bit.ly/woo-customer-coupons): Display coupons on your website

[Custom Email Blocks for WooCommerce](http://bit.ly/woo-custom-email-blocks): Create your own professional email design & content for all your outgoing emails

[Virtual Reviews for WooCommerce](http://bit.ly/woo-virtual-reviews): Virtual Reviews for WooCommerce helps generate virtual reviews, display canned reviews for newly created store

[Thank You Page Customizer for WooCommerce](http://bit.ly/woo-thank-you-page-customizer): Customize your “Thank You” page and give coupons to customers after a successful order

[EU Cookies Bar](http://bit.ly/eu-cookies-bar): A very simple plugin which helps your website comply with Cookie Law

[Lucky Wheel for WooCommerce](http://bit.ly/woo-lucky-wheel): Offer customers to spin for coupons by entering their emails.

[WordPress Lucky Wheel](http://bit.ly/wp-lucky-wheel): WordPress Lucky Wheel gives you the best solution to get emails address from visitors of your WordPress website

[Advanced Product Information for WooCommerce](http://bit.ly/woo-advanced-product-information): Display more intuitive information of products such as sale countdown, sale badges, who recently bought products, rank of products in their categories, available payment methods...

[LookBook for WooCommerce](http://bit.ly/woo-lookbook): Create beautiful Lookbooks, shop by Instagram.

[Photo Reviews for WooCommerce](http://bit.ly/woo-photo-reviews): Allow posting reviews include product pictures, review reminder, review for coupons.

[Product Builder for WooCommerce](http://bit.ly/woo-product-builder): Allows your customers to build a full product set from small parts step by step. The plugin works base on WooCommerce with many useful features like compatible, email completed product, attributes filters.

[Boost Sales for WooCommerce](http://bit.ly/woo-boost-sales): Increase profit on every single order with Up-selling and Cross-selling

[Free Shipping Bar for WooCommerce](http://bit.ly/woo-free-shipping-bar): Use free shipping as a marketing tool, encourage customers to pay more for free shipping.

[Notification for WooCommerce](http://bit.ly/woo-notification): Social Proof Marketing plugin. Live recent order on the front-end of your site.

[Multi Currency for WooCommerce](http://bit.ly/woo-multi-currency): Switches to different currencies easily and accepts payment with only one currency or all currencies.

[Coupon Box for WooCommerce](http://bit.ly/woo-coupon-box-free): Subscribe emails for discount coupons

### Documentation

- [Getting Started](http://docs.villatheme.com/?item=sales-countdown-timer)

### Plugin Links

- [Project Page](https://villatheme.com)
- [Documentation](http://docs.villatheme.com/?item=sales-countdown-timer)
- [Report Bugs/Issues](https://villatheme.com/supports)

== Installation ==

1. Unzip the download package
1. Upload `sales-countdown-timer` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==
1. Countdown timer designs
2. Countdown timer with progress bar on WooCommerce single products, category page and shop page settings

== Changelog ==
/*1.0.4.2 - 2019.04.03*/
- Updated: Make admin notices dismissible

/*1.0.4.1 - 2019.01.03*/
- Fixed: Product quantity broken

/*1.0.4 - 2018.12.04*/
- Fixed: Products price when start and end sales
- Removed: Option to show on related products
- Added: Option to set size of countdown timer on shop/category page and on mobile
- Added: Datetime unit position
- Added: Animation style
- Added: Circle countdown timer
- Added: Make countdown timer sticky when scroll on single product page

/*1.0.3.1 - 2018.11.21*/
- Fixed: Current countdown timer style not working in admin settings

/*1.0.3 - 2018.11.21*/
- Updated: Working with variable products
- Optimized: Speed load

/*1.0.2.1 - 2018.11.10*/
- Updated: Class support

/*1.0.2 - 2018.10.17*/
- Updated: Class support

/*1.0.1 - 2018.10.15*/
- Fixed: Class support
