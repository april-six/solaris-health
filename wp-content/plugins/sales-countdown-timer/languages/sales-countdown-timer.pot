#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Sales Countdown Timer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-04 04:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Name of the plugin
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:30
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:40
msgid "Sales Countdown Timer"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:30
msgid "Countdown Timer"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:89
msgid "See your very first sales countdown timer "
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:89
msgid "here."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:124
msgid "From: "
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:128
msgid "To: "
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:134
msgid "Shortcode: "
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:137
msgid "Copy shortcode"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:138
msgid "Duplicate"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:139
msgid "Remove"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:147
msgid "General settings"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:154
msgid "Name"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:162
msgid "Schedule time for shortcode usage"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:166
msgid ""
"These values are used for shortcode only. To schedule sale for product "
"please go to admin product."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:168
msgid "From"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:187
msgid "To"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:208
msgid "Design"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:222
msgid "Message"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:230
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:864
msgid "The countdown timer that you set on tab design"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:233
msgid ""
"The countdown timer will not show if message does not include "
"{countdown_timer}"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:237
msgid "Time separator"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:240
msgid "Blank"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:241
msgid "Colon(:)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:242
msgid "Comma(,)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:243
msgid "Dot(.)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:247
msgid "Datetime format style"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:250
msgid "01 days 02 hrs 03 mins 04 secs"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:251
msgid "01 days 02 hours 03 minutes 04 seconds"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:252
msgid "01:02:03:04"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:253
msgid "01d:02h:03m:04s"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:263
msgid "Datetime unit position"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:266
msgid "Top"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:267
msgid "Bottom"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:271
msgid "Animation style"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:274
msgid "Default"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:275
msgid "Slide"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:282
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:288
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:168
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:174
msgid "days"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:283
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:169
msgid "hrs"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:284
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:170
msgid "mins"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:285
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:171
msgid "secs"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:289
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:175
msgid "hours"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:290
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:176
msgid "minutes"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:291
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:177
msgid "seconds"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:300
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:186
msgid "d"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:301
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:187
msgid "h"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:302
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:188
msgid "m"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:303
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/frontend/shortcode.php:189
msgid "s"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:309
msgid "Display type"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:342
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:406
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:467
msgid "01"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:350
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:414
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:474
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:541
msgid "02"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:358
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:422
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:481
msgid "03"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:366
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:430
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:488
msgid "04"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:527
msgid "10"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:555
msgid "30"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:569
msgid "40"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:598
msgid "Use smooth animation for circle"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:600
msgid ""
"(*)Countdown timer items Border radius, Height and Width are not applied to "
"this type."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:607
msgid "Countdown timer"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:611
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:702
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:733
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:971
msgid "Color"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:619
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:710
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:741
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:963
msgid "Background"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:627
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:660
msgid "Border color"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:636
msgid "Padding(px)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:644
msgid "Border radius"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:656
msgid "Countdown timer items"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:668
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:979
msgid "Border radius(px)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:676
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:954
msgid "Height(px)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:684
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:947
msgid "Width(px)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:698
msgid "Datetime value"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:718
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:749
msgid "Font size(px)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:729
msgid "Datetime unit"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:762
msgid "WooCommerce Product"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:769
msgid "Make countdown timer sticky when scroll"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:779
msgid "Position on single product page"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:781
msgid ""
"Position of countdown timer of main product on single product page(Can not "
"set position for variations)"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:784
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:800
msgid "Before price"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:785
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:801
msgid "After price"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:786
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:802
msgid "Before sale flash"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:787
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:803
msgid "After sale flash"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:788
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:804
msgid "Before cart"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:789
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:805
msgid "After cart"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:790
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:806
msgid "Product image"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:795
msgid "Position on archive page"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:797
msgid ""
"Position of countdown timer on shop page, category page and related products"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:813
msgid "Show on shop page"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:823
msgid "Show on category page"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:833
msgid ""
"Reduce size of countdown timer on shop/category page and on mobile(single "
"product) by"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:845
msgid "Upcoming sale"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:848
msgid "Enable"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:858
msgid "Upcoming sale message"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:870
msgid "Progress bar"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:872
msgid "Progress bar message"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:892
msgid "Progress bar type"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:894
msgid ""
"If select increase, the progress bar fill will increase each time the "
"product is bought and vice versa"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:898
msgid "Increase"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:899
msgid "Decrease"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:904
msgid "Order status"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:909
msgid ""
"When new order created, update the progress bar when order status are(leave "
"blank to apply for all order status):"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:914
msgid "Completed"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:917
msgid "On-hold"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:920
msgid "Pending"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:923
msgid "Processing"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:926
msgid "Failed"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:929
msgid "Refunded"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:932
msgid "Cancelled"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:939
msgid "Position"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:942
msgid "Above Countdown"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:943
msgid "Below Countdown"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1002
msgid "Save"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1007
msgid "Settings saved"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1074
msgid "Can not remove all Countdown timer settings."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1079
msgid "Names are unique."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1085
msgid "Names can not be empty."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1093
msgid "Data saved."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/admin.php:1240
msgid "Settings"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:218
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:272
msgid "Countdown timer profile"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:221
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:275
msgid "Select countdown timer settings."
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:231
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:286
msgid "Goal"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:231
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:286
msgid "Your product goal"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:232
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:287
msgid "Initial quantity"
msgstr ""

#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:232
#: E:/Git/VILLAT~1/FREE-V~1/SALES-~1/sales-countdown-timer/admin/product.php:287
msgid "This is the virtual quantity of sold products"
msgstr ""

#. Description of the plugin
msgid ""
"Create a sense of urgency with a countdown to the beginning or end of sales, "
"store launch or other events for higher conversions."
msgstr ""

#. URI of the plugin
msgid "https://villatheme.com/extensions/sales-countdown-timer/"
msgstr ""

#. Author of the plugin
msgid "VillaTheme"
msgstr ""

#. Author URI of the plugin
msgid "http://villatheme.com"
msgstr ""
