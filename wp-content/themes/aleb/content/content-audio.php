<?php $sticky_class = is_sticky()?'sticky':''; ?>

<?php 
$events_header_style = (get_post_meta($wp_query->get_queried_object_id(), "events_met_header_style", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_header_style", true) : 'alway_sticky';
$events_post_bg_heading = (get_post_meta($wp_query->get_queried_object_id(), "events_met_post_bg_heading", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_post_bg_heading", true) : 'nobgheading';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-wrap '. $sticky_class); ?>  >
		
		<?php if(has_post_format('audio')){ ?>
        <div class="post-media">
        	<?php events_postformat_audio(); /* Display video of post */ ?>
        </div>
        <?php } ?>

        <?php if( !is_single() || ($events_header_style == 'alway_sticky' && $events_post_bg_heading == 'nobgheading') ){ ?> 
        <div class="post-title">
	            <?php events_content_title(); /* Display title of post */ ?>
	    </div>
	    <?php } ?>

	    

	    <?php if( !is_single() || ($events_header_style == 'alway_sticky' && $events_post_bg_heading == 'nobgheading') ){ ?> 
        <div class="post-meta">
	        <?php events_content_meta(); /* Display Date, Author, Comment */ ?>
	    </div>
	    <?php } ?>

	    <div class="post-body">
	            <?php events_content_body(); /* Display content of post or intro in category page */ ?>
	    </div>

	    <?php if(!is_single()){ ?> 
	            <?php events_content_readmore(); /* Display read more button in category page */ ?>
	    <?php }?>

	    <?php if(is_single()){ ?>
	    <?php events_content_tag(); /* Display tags, category */ ?>
	    <?php } ?>

</article>