
<article id="post-<?php the_ID(); ?>" class="result_search">
        <div class="post-title">
	            <?php events_content_title(); /* Display title of post */ ?>
	    </div>
	    <div class="post-body">
	            <?php events_content_body(); /* Display content of post or intro in category page */ ?>
	    </div>

</article>
