	<footer class="footer">
		<?php if(get_theme_mod("events_cus_footer_idslider","yes") != "no"){ ?>
			<div class="scrolltop"><a href="#home" class="totop"><i class="fa fa-angle-up"></i></a></div>
		<?php } ?>
		<div class="container">
			<div class="col-md-12">
				<?php if( is_active_sidebar('footer') ){ dynamic_sidebar('footer'); } ?>
			</div>
		</div>
	</footer>

	</div> <!-- /Wrapper -->
</div> <!-- /container_boxed -->
    
<?php wp_footer(); ?>
</body>
</html>