<?php	
function events_register_menus() {
  register_nav_menus( array(
    'onepage'   => esc_html__( 'One Page Menu', 'events' ),
    'primary'   => esc_html__( 'Primary Menu', 'events' )
  ) );
}
add_action( 'init', 'events_register_menus' );
