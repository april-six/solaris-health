<?php

/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

if (file_exists(OVA_THEME_URL . '/framework/metabox/init.php')) {
    require_once(OVA_THEME_URL . '/framework/metabox/init.php');
}

add_action('cmb2_init', 'events_metaboxes');
function events_metaboxes()
{

    // Start with an underscore to hide fields from custom fields list
    $prefix = 'events_met_';

   

    /* Page settings */
    $cmb = new_cmb2_box(array(
        'id' => 'page_heading_settings',
        'title' => esc_html__('Show Page Heading', 'events'),
        'object_types' => array('page'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,

    ));

            // Display title of page
    $cmb->add_field(array(
        'name' => esc_html__('Show title of page', 'events'),
        'desc' => esc_html__('Allow display title of page', 'events'),
        'id' => $prefix . 'page_heading',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'show' => esc_html__('Show', 'events'),
            'hide' => esc_html__('Hide', 'events')
        ),
        'default' => 'show',

    ));

            
           



    /* Post settings */
    $cmb = new_cmb2_box(array(
        'id' => 'post_video',
        'title' => esc_html__('Post Settings', 'events'),
        'object_types' => array('post'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left

    ));

            // Video or Audio
    $cmb->add_field(array(
        'name' => esc_html__('Link audio or video', 'events'),
        'desc' => esc_html__('Insert link audio or video use for video/audio post-format', 'events'),
        'id' => $prefix . 'embed_media',
        'type' => 'oembed',
    ));

            // Gallery image
    $cmb->add_field(array(
        'name' => esc_html__('Gallery image', 'events'),
        'desc' => esc_html__('image in gallery post format', 'events'),
        'id' => $prefix . 'file_list',
        'type' => 'file_list',
    ));

            // thumbnail image
    $cmb->add_field(array(
        'name' => esc_html__('Thumbnail image', 'events'),
        'desc' => esc_html__('Thumbnail image that display with blog shortcode in homepage', 'events'),
        'id' => $prefix . 'post_file',
        'type' => 'file',
        'allow' => array('url', 'attachment')
    ));

    

    /* Layout Settings */
    $cmb = new_cmb2_box(array(
        'id' => 'layout_settings',
        'title' => esc_html__('General Settings', 'events'),
        'object_types' => array('page', 'post', 'schedule'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left

    ));
    $cmb->add_field(array(
        'name' => esc_html__('Choose theme location to display menu', 'events'),
        'id' => $prefix . 'location_theme',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'primary' => esc_html__('Primary', 'events'),
            'onepage' => esc_html__('One page', 'events')
        ),
        'default' => 'primary',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Choose Header', 'events'),
        'id' => $prefix . 'header_style',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'header_default' => esc_html__('Header default', 'events'),
            'header_transparent' => esc_html__('Header transparent', 'events'),
            'header_bg' => esc_html__('Header background image', 'events')
        ),
        'default' => 'header_default',
    ));

            // thumbnail image
    $cmb->add_field(array(
        'name' => esc_html__('Background heading', 'events'),
        'desc' => esc_html__('You have to choose a background', 'events'),
        'id' => $prefix . 'general_bg_heading',
        'type' => 'file',
        'allow' => array('url', 'attachment')
    ));

            /* Title */
    $cmb->add_field(array(
        'name' => esc_html__('Title in heading', 'events'),
        'id' => $prefix . 'general_title_heading',
        'type' => 'textarea'
    ));
            /* subtitle */
    $cmb->add_field(array(
        'name' => esc_html__('Sub title in heading', 'events'),
        'id' => $prefix . 'general_subtitle_heading',
        'type' => 'textarea'
    ));

    $cmb->add_field(array(
        'name' => esc_html__('Background color for menu', 'events'),
        'id' => $prefix . 'bgcolor_menu',
        'type' => 'colorpicker',
        'desc' => esc_html__('Insert transparent to display transparent-background', 'events')
    ));


    $cmb->add_field(array(
        'name' => esc_html__('Main Layout', 'events'),
        'desc' => esc_html__('This value will override value in theme customizer', 'events'),
        'id' => $prefix . 'main_layout',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'global' => esc_html__('Global in customizer', 'events'),
            'right_sidebar' => esc_html__('Right Sidebar', 'events'),
            'left_sidebar' => esc_html__('Left Sidebar', 'events'),
            'fullwidth' => esc_html__('No Sidebar', 'events'),
        ),
        'default' => 'global',

    ));


    $cmb->add_field(array(
        'name' => esc_html__('Width of site', 'events'),
        'desc' => esc_html__('This value will override value in theme customizer', 'events'),
        'id' => $prefix . 'version_layout',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'global' => esc_html__('Global in customizer', 'events'),
            'wide' => esc_html__('Wide', 'events'),
            'boxed' => esc_html__('Boxed', 'events'),
        ),
        'default' => 'global',

    ));

            // Seo keywords
    $cmb->add_field(array(
        'name' => esc_html__('SEO Keywords', 'events'),
        'desc' => esc_html__('(optional). This value will override value in global seo keyword of theme customizer.', 'events'),
        'id' => $prefix . 'seo_key',
        'type' => 'text',

    ));

            // Seo description
    $cmb->add_field(array(
        'name' => esc_html__('SEO Description', 'events'),
        'desc' => esc_html__('(optional). This value will override value in global seo description of theme customizer.', 'events'),
        'id' => $prefix . 'seo_desc',
        'type' => 'textarea',
    ));

    
    /* Slideshow Settings */
    $cmb = new_cmb2_box(array(
        'id' => 'slideshow_settings',
        'title' => esc_html__('Slideshow Settings', 'events'),
        'object_types' => array('slideshow'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Choose template', 'events'),
        'id' => $prefix . 'slideshow_choose_template',
        'type' => 'select',
        'show_option_none' => false,
        'options' => array(
            'basic' => esc_html__('Basic', 'events'),
            'countdown' => esc_html__('Countdown', 'events'),
            'register' => esc_html__('Register', 'events'),

        ),
        'default' => 'basic',
    ));

    $cmb->add_field(array(
        'name' => esc_html__('Background slide', 'events'),
        'id' => $prefix . 'slideshow_bg',
        'type' => 'file',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Image', 'events'),
        'id' => $prefix . 'slideshow_image',
        'type' => 'file',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Title', 'events'),
        'id' => $prefix . 'slideshow_title',
        'type' => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Sub Title', 'events'),
        'id' => $prefix . 'slideshow_subtitle',
        'type' => 'text',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Description', 'events'),
        'id' => $prefix . 'slideshow_desc',
        'type' => 'textarea',

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Countdown shortcode', 'events'),
        'id' => $prefix . 'slideshow_countdown_shortcode',
        'type' => 'textarea',
        'description' => esc_html__('Countdown shortcode like: [events_countdown end_day="05" end_month="1" end_year="2016" display_format="dHMS" timezone="0" years="years" months="months" weeks="weeks" days="days" hours="hours" minutes="minutes" seconds="seconds" year="year" month="month" week="week" day="day" hour="hour" minute="minute" second="second"  animation_delay="0" class="" /] .You will find shortcode detail in the documentation', 'events'),

    ));

    $cmb->add_field(array(
        'name' => esc_html__('Button shortcode', 'events'),
        'id' => $prefix . 'slideshow_button_shortcode',
        'type' => 'textarea',
        'description' => esc_html__('Button shortcode like: [events_button name="WATCH VIDEO" link="https://www.youtube.com/watch?v=nrJtHemSPW4" target="popup_video" bg="transparent" bg_hover="#44cb9a" text_color="#fff" text_color_hover="#fff" icon="fa-chevron-circle-right" border_radius="4px" border_color="#fff" border_color_hover="#44cb9a"  margin="2px 5px" animation_delay="0" class="" /] .You will find shortcode detail in the documentation', 'events'),

    ));



    $cmb->add_field(array(
        'name' => esc_html__('Register shortcode', 'events'),
        'id' => $prefix . 'slideshow_register_shortcode',
        'type' => 'textarea',
        'description' => esc_html__('Register shortcode like: [events_registerform type="free" title="Register Free Form" subtitle="* We process using a 100% secure gateway" style="style1" buttontext="REGISTER FORM" iconbutton="fa-arrow-circle-o-right" id="" bg_button="#f74949" bg_button_hover="#fff" text_button_color="#fff" text_button_color_hover="#f74949" animation_delay="0" class=""  /] .You will find shortcode detail in the documentation', 'events'),

    ));


    /* Schedule Settings */
    $cmb = new_cmb2_box(array(
        'id' => 'schedule_settings',
        'title' => esc_html__('Schedule Settings', 'events'),
        'object_types' => array('schedule'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,

    ));
            // thumbnail image
    $cmb->add_field(array(
        'name' => esc_html__('Background heading', 'events'),
        'desc' => esc_html__('You have to choose a background', 'events'),
        'id' => $prefix . 'schedule_bg_heading',
        'type' => 'file',
        'allow' => array('url', 'attachment')
    ));

            /* Image */
    $cmb->add_field(array(
        'name' => esc_html__('Image', 'events'),
        'desc' => esc_html__('Choose an Image', 'events'),
        'id' => $prefix . 'schedule_Image',
        'type' => 'file',
        'allow' => array('url', 'attachment')
    ));

            /* Title */
    $cmb->add_field(array(
        'name' => esc_html__('Title in heading', 'events'),
        'id' => $prefix . 'schedule_title_heading',
        'type' => 'textarea'
    ));
            /* subitle */
    $cmb->add_field(array(
        'name' => esc_html__('Sub title in heading', 'events'),
        'id' => $prefix . 'schedule_subtitle_heading',
        'type' => 'textarea'
    ));
    $cmb->add_field(array(
        'name' => esc_html__('Time Text', 'events'),
        'desc' => esc_html__('For example: 45 minutes', 'events'),
        'id' => $prefix . 'schedule_timetext',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('icon for time', 'events'),
        'desc' => esc_html__('For example: fa-clock-o', 'events'),
        'id' => $prefix . 'schedule_timeicon',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('intermediate', 'events'),
        'desc' => esc_html__('Intermediate', 'events'),
        'id' => $prefix . 'schedule_intermediate',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('icon for Intermediate', 'events'),
        'desc' => esc_html__('For example: fa-signal', 'events'),
        'id' => $prefix . 'speaker_intermediate_icon',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('Author', 'events'),
        'desc' => esc_html__('Author will speak: John Doe', 'events'),
        'id' => $prefix . 'schedule_author',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('Author Job', 'events'),
        'desc' => esc_html__('Job of author: CEO at Crown.io', 'events'),
        'id' => $prefix . 'schedule_author_job',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => esc_html__('Link title of speaker', 'events'),
        'desc' => esc_html__('Insert link for speaker', 'events'),
        'id' => $prefix . 'schedule_link_speaker',
        'type' => 'text',
    ));




}

