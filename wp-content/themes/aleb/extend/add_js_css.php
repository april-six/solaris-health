<?php 

add_action('wp_head', 'events_primary_color');
add_action('wp_enqueue_scripts', 'events_script_ie');
add_action('wp_enqueue_scripts', 'events_theme_scripts_styles');
add_action('wp_enqueue_scripts', 'events_google_fonts');



function events_theme_scripts_styles() {

    
    /* Add Javascript bellow - use events_add_js() to add: $name - unique, $url - path of javascript */
    events_add_js('bootstrap-3.3.5-dist', OVA_THEME_URI.'/assets/plugins/bootstrap-3.3.5-dist/js/bootstrap.min.js');
    events_add_js('owlcarousel2', OVA_THEME_URI.'/assets/plugins/owlcarousel2/owl.carousel.min.js');
    events_add_js('countdown_jquery', OVA_THEME_URI.'/assets/plugins/countdown/jquery.plugin.min.js');
    events_add_js('countdown', OVA_THEME_URI.'/assets/plugins/countdown/jquery.countdown.js');

    $protocol = is_ssl() ? 'https' : 'http';

    wp_enqueue_style('jquery-style', $protocol.'://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    
    events_add_js('ajax-script', OVA_THEME_URI.'/assets/js/registration.js');
    if(!is_admin()){
      wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    }
    
    events_add_js('waypoints', OVA_THEME_URI.'/assets/plugins/waypoints/jquery.waypoints.min.js');
    events_add_js('map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyALw-8KDZPw976zDr6U1LvU7YgHFmDP4Iw');

    events_add_js('prettyphoto', OVA_THEME_URI.'/assets/plugins/prettyPhoto/jquery.prettyPhoto.js');
    events_add_js('superfish', OVA_THEME_URI.'/assets/plugins/superfish/js/superfish.js');
    events_add_js('onepagemenu', OVA_THEME_URI.'/assets/plugins/jquery.nav.js');
    

    events_add_js('theme', OVA_THEME_URI.'/assets/js/theme.js');
    events_add_js('theme-init', OVA_THEME_URI.'/assets/js/theme_init.js');


    /* Add Css bellow - uyse events_add_css to add: $name - unique, $url - path of css*/
    events_add_css('bootstrap-3.3.5-dist', OVA_THEME_URI.'/assets/plugins/bootstrap-3.3.5-dist/css/bootstrap.min.css');
    events_add_css('font-awesome-4.4.0', OVA_THEME_URI.'/assets/plugins/font-awesome-4.4.0/css/font-awesome.min.css');
    events_add_css('owlcarousel2', OVA_THEME_URI.'/assets/plugins/owlcarousel2/assets/owl.carousel.min.css');
    events_add_css('animate', OVA_THEME_URI.'/assets/css/animate.css');
    
    events_add_css('superfish-css', OVA_THEME_URI.'/assets/plugins/superfish/css/superfish-navbar.css');
    events_add_css('fix_superfish', OVA_THEME_URI.'/assets/css/fix_superfish.css');
    events_add_css('prettyphoto_css', OVA_THEME_URI.'/assets/plugins/prettyPhoto/prettyPhoto.css');



    events_add_css('fix', OVA_THEME_URI.'/assets/css/fix.css');

    if ( is_child_theme() ) {
      wp_enqueue_style( 'parent-stylesheet', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
    }
    
    wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), 'events' );

}

function events_primary_color(){ ?>

         <style type="text/css">

            <?php
                $main_color = get_theme_mod('events_cus_main_color', '#f74949');
                $cusbodyfont = get_theme_mod('events_cus_body_font', 'Roboto');

                $cusheading = get_theme_mod('events_cus_heading', 'Montserrat');

                $events_cus_logo_color = get_theme_mod('events_cus_logo_color','#fff');
                $events_cus_stickylogo_color = get_theme_mod('events_cus_stickylogo_color','#2f343a');

                $events_cus_menu_color = get_theme_mod('events_cus_menu_color', '#fff');
                $events_cus_menushrink_color = get_theme_mod('events_cus_menushrink_color', '#ffffff');
                $events_cus_bgmenushrink_color = get_theme_mod('events_cus_bgmenushrink_color', '#000000');
            ?>

            /* Loading */            
            .timer-loader:not(:required) {
                border-color: <?php echo esc_attr($main_color); ?>;
            }
            .timer-loader:not(:required)::before {
                background: <?php echo esc_attr($main_color); ?>;
            }
            .timer-loader:not(:required)::after {
                background: <?php echo esc_attr($main_color); ?>;
            }

            @-webkit-keyframes timer-loader {
              0%,
              100% {
                box-shadow: 0 -3em 0 0.2em <?php echo esc_attr($main_color); ?>, 2em -2em 0 0em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 0 <?php echo esc_attr($main_color); ?>;
              }
              12.5% {
                box-shadow: 0 -3em 0 0 <?php echo esc_attr($main_color); ?>, 2em -2em 0 0.2em <?php echo esc_attr($main_color); ?>, 3em 0 0 0 <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              25% {
                box-shadow: 0 -3em 0 -0.5em <?php echo esc_attr($main_color); ?>, 2em -2em 0 0 <?php echo esc_attr($main_color); ?>, 3em 0 0 0.2em <?php echo esc_attr($main_color); ?>, 2em 2em 0 0 <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              37.5% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0em 0 0 <?php echo esc_attr($main_color); ?>, 2em 2em 0 0.2em <?php echo esc_attr($main_color); ?>, 0 3em 0 0em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              50% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 0em <?php echo esc_attr($main_color); ?>, 0 3em 0 0.2em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              62.5% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 0 <?php echo esc_attr($main_color); ?>, -2em 2em 0 0.2em <?php echo esc_attr($main_color); ?>, -3em 0 0 0 <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              75% {
                box-shadow: 0em -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 0.2em <?php echo esc_attr($main_color); ?>, -2em -2em 0 0 <?php echo esc_attr($main_color); ?>;
              }
              87.5% {
                box-shadow: 0em -3em 0 0 <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 0 <?php echo esc_attr($main_color); ?>, -2em -2em 0 0.2em <?php echo esc_attr($main_color); ?>;
              }
            }
            @keyframes timer-loader {
              0%,
              100% {
                box-shadow: 0 -3em 0 0.2em <?php echo esc_attr($main_color); ?>, 2em -2em 0 0em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 0 <?php echo esc_attr($main_color); ?>;
              }
              12.5% {
                box-shadow: 0 -3em 0 0 <?php echo esc_attr($main_color); ?>, 2em -2em 0 0.2em <?php echo esc_attr($main_color); ?>, 3em 0 0 0 <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              25% {
                box-shadow: 0 -3em 0 -0.5em <?php echo esc_attr($main_color); ?>, 2em -2em 0 0 <?php echo esc_attr($main_color); ?>, 3em 0 0 0.2em <?php echo esc_attr($main_color); ?>, 2em 2em 0 0 <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              37.5% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0em 0 0 <?php echo esc_attr($main_color); ?>, 2em 2em 0 0.2em <?php echo esc_attr($main_color); ?>, 0 3em 0 0em <?php echo esc_attr($main_color); ?>, -2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, -3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              50% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 0em <?php echo esc_attr($main_color); ?>, 0 3em 0 0.2em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              62.5% {
                box-shadow: 0 -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 0 <?php echo esc_attr($main_color); ?>, -2em 2em 0 0.2em <?php echo esc_attr($main_color); ?>, -3em 0 0 0 <?php echo esc_attr($main_color); ?>, -2em -2em 0 -1em <?php echo esc_attr($main_color); ?>;
              }
              75% {
                box-shadow: 0em -3em 0 -1em <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0em 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 0.2em <?php echo esc_attr($main_color); ?>, -2em -2em 0 0 <?php echo esc_attr($main_color); ?>;
              }
              87.5% {
                box-shadow: 0em -3em 0 0 <?php echo esc_attr($main_color); ?>, 2em -2em 0 -1em <?php echo esc_attr($main_color); ?>, 3em 0 0 -1em <?php echo esc_attr($main_color); ?>, 2em 2em 0 -1em <?php echo esc_attr($main_color); ?>, 0 3em 0 -1em <?php echo esc_attr($main_color); ?>, -2em 2em 0 0 <?php echo esc_attr($main_color); ?>, -3em 0em 0 0 <?php echo esc_attr($main_color); ?>, -2em -2em 0 0.2em <?php echo esc_attr($main_color); ?>;
              }
            }
            /* /Loading */ 

            /* Font-family default Montserrat */
            h1,h2,h3,h4,h5,h6,
            .events_countdown .countdown-section,
            .main_slider .item .title,
            .main_slider .item .sub_title,
            #tabs-lv1 li a,
            .schedule_timeline .info_left .speaker_info .author,
            .events_price .price_title,
            .events_price .price_amount,
            .events-google-map .marker_title,
            .commentlists .author-name .name_author a,
            .events_speakers .media-body h4.media-heading,
            .topics_covered .media-heading a,
            button.submit-button,
            .subscribe_form input.submit,
            .events_contactform .events_submit,
            .sf-menu a,
            .page-section article.post-wrap .post-readmore a,
            .comment-form input[type="submit"],
            .contact_info .info,
            .schedule_timeline .item h2.post-title a,
            .schedule_timeline .info_left .speaker_info .author a,
            .events_price .price_value sub,
            .nearby_accomodation .media-heading,
            .ovatheme_form.style1 .title_form,
            .events-google-map .marker_title h4,
            .quickinfo .quick_content .title,
            .quickinfo .quick_content .description,
            .event_faq .vc_toggle_title,
            .faq_sec .vc_tta-panel-title>a
            
            { 
              font-family: <?php echo esc_attr($cusheading); ?>, sans-serif;
              
            }
            .cbp-l-caption-title{
                font-family: <?php echo esc_attr($cusheading); ?>, sans-serif!important;
                letter-spacing: 0.2em!important;
                font-size: 14px!important;
                text-align: center!important;
                text-transform: uppercase!important;
            }

            .events_heading h2{
                font-family: <?php echo esc_attr($cusheading); ?>, sans-serif;
            }
            .sc_button a{
                font-family: <?php echo esc_attr($cusheading); ?>, sans-serif;
            }
            /* /Font-family default Montserrat */

            /* cusbodyfont default Roboto */
            body,
            .main_slider .item .desc,
            .events_heading h3,
            #tabs-lv20 li a,
            .events_speakers .media-heading .media-info,
            .ovatheme_form input,
            .ovatheme_form textarea,
            .ovatheme_form select,
            .quickinfo .quick_content .time
            { 
              font-family: <?php echo esc_attr($cusbodyfont); ?>, sans-serif;
              font-weight: 300;
              
            }
            #cbpw-grid2 .cbp-l-caption-desc{
                font-family: <?php echo esc_attr($cusbodyfont); ?>, sans-serif!important;
                text-align: center!important;
            }

            .events-schedule-tabs.lv2 ul li.active{
            
                border-bottom-color: <?php echo esc_attr($main_color); ?>;
            }
            .tooltip-inner{
                background-color: <?php echo esc_attr($main_color); ?>;
            }
            .events-schedule-tabs.lv1 li.active a,
            .events-schedule-tabs.lv1 li.active a:hover,
            .schedule_timeline .info_left .speaker_info a:hover,
            .schedule_timeline .item h2.post-title a:hover,
            .topics_covered .media-other_desc li .fa{
                color: <?php echo esc_attr($main_color); ?>;
            }
            .events-schedule-tabs.lv1 .nav>li>a:focus,
            .events-schedule-tabs.lv2 .nav>li>a:focus{
                background-color: transparent;
            }

            .schedule_single .quick_speaker .time,
            .schedule_single .quick_speaker .intermediate{
                color: <?php echo esc_attr($main_color); ?>;
            }
            .from_our_blog .post-footer .post-readmore a,
            .nearby_accomodation .media-readmore a,
            .getintouch{
                border-color: <?php echo esc_attr($main_color); ?>;
            }



            .sf-menu li:hover .show_dropmenu{
                color: <?php echo esc_attr($main_color); ?>;
            }
            header ul.sf-menu li a{
                color: <?php echo esc_attr($events_cus_menu_color); ?>;   
            }
            header ul.sf-menu li a:hover,
            header ul.sf-menu li.current a{
                color: <?php echo esc_attr($main_color); ?>;   
            }
            .sf-menu>li.current-menu-item>a>.af_border, 
            .sf-menu>li>a:hover>.af_border{
                    border-bottom-color: <?php echo esc_attr($main_color); ?>;  
            }

            header.shrink{
                background-color: <?php echo esc_attr($events_cus_bgmenushrink_color); ?>!important;
            }



            /*header.alway_sticky,*/
            header.shrink{
                background-color: <?php echo esc_attr($events_cus_bgmenushrink_color); ?>; 
            }

            .sf-menu>li.current-menu-item>a{
                color: <?php echo esc_attr($main_color); ?>!important;
            }
            .sf-menu>li.current-menu-item>a>.af_border{
                border-bottom: 2px solid <?php echo esc_attr($main_color); ?>;
            }


             .dropdown-menu>.active>a,
             header.fixed ul.sf-menu .dropdown-menu li.current a:hover,
             header .sf-menu .dropdown-menu a:hover{
                background-color: #f3f3f3;
                color: #5c5c5c!important;
             }

              header.shrink ul.sf-menu li a{
                color: <?php echo esc_attr($events_cus_menushrink_color); ?>;
             }
             header.shrink ul.sf-menu li.current a{
                color: <?php echo esc_attr($main_color); ?>;
             }



            .address .media-desc,.events_speckers .media-heading .media-info,
            .events_registernow_text .register_text h5,
            .venue_location h4, .venue_location h5,
            .from_our_blog .post-title{
                font-family: <?php echo esc_attr($cusheading); ?>, sans-serif;
                font-weight: 300;
            }

            .topics_covered .media-title,
            .events_registernow_text .register_text h3{
                font-family: <?php echo esc_attr($cusheading); ?>, sans-serif; 
            }

            .square_countdown .events_countdown .countdown-section:nth-child(2),
            .square_countdown .events_countdown .countdown-section:nth-child(4){
                background-color: <?php echo esc_attr($main_color); ?>;
            }

            a:hover{
                color: <?php echo esc_attr($main_color); ?>; 
            }
            .events_registernow_text .register_text h3 a:hover{
                color: <?php echo esc_attr($main_color); ?>;    
            }
            .nearby_accomodation .price{
                background-color: <?php echo esc_attr($main_color); ?>;   
            }
            .nearby_accomodation .price:before{
                border-top-color: <?php echo esc_attr($main_color); ?>;
            }
            .nearby_accomodation .price:after{
                border-bottom-color: <?php echo esc_attr($main_color); ?>;   
            }
            .nearby_accomodation .media-heading a:hover{
                color: <?php echo esc_attr($main_color); ?>;
            }


            

             .events_heading hr{
                border-top: 3px solid <?php echo esc_attr($main_color); ?>;   
                color: <?php echo esc_attr($main_color); ?>;
             }
             .events_heading.sponsor_heading  hr{
                border-top: 3px solid <?php echo esc_attr($main_color); ?>;   
                color: <?php echo esc_attr($main_color); ?>;
             }

             .events_bgslide .owl-controls .owl-next{
                background-color: <?php echo esc_attr($main_color); ?>;  
             }

            .events_speakers h4.media-heading a:hover,
            .events_speakers .media-social a:hover i{
                color:<?php echo esc_attr($main_color); ?>!important;
            }
            .from_our_blog .post-title a:hover,
            .from_our_blog .post-footer .post-readmore a
            {
                color:<?php echo esc_attr($main_color); ?>;   
            }
            .contact_info .icon
            {
                border-color: <?php echo esc_attr($main_color); ?>;
            }
            .contact_info .icon .fa{
                color: <?php echo esc_attr($main_color); ?>;   
            }
            .topics_covered a:hover{
               color:<?php echo esc_attr($main_color); ?>!important;
            }
            .frequently_questions_item a:hover{
                color:<?php echo esc_attr($main_color); ?>!important;   
            }

            .events_contactform .button .events_submit:hover{
                background-color: <?php echo esc_attr($main_color); ?>!important;
            }

            footer.footer ul.social li a:hover{
                color: <?php echo esc_attr($main_color); ?>;
                border-color: <?php echo esc_attr($main_color); ?>;
            }

            .page-section article.post-wrap .post-title a:hover,
            .page-section article.post-wrap .post-meta .post-author .right a,
            article.post-wrap .post-meta .comment a:hover,
            #sidebar .widget a:hover
            {
                color: <?php echo esc_attr($main_color); ?>;
            }
            .page-section article.post-wrap .post-readmore a:hover,
            .sidebar h4.widget-title:before
            {
                background-color: <?php echo esc_attr($main_color); ?>;
            }
            #sidebar .widget_tag_cloud .tagcloud a:hover{
                background-color: <?php echo esc_attr($main_color); ?>;
                border-color: <?php echo esc_attr($main_color); ?>;
            }

            .sidebar .events_social_icon a:hover{
                color: <?php echo esc_attr($main_color); ?>;
                border-color: <?php echo esc_attr($main_color); ?>;   
            }
            .sidebar .events_social_icon a:hover i{
                color: <?php echo esc_attr($main_color); ?>!important;
            }
            .single .post-tag .post-tags a:hover{
                border-color: <?php echo esc_attr($main_color); ?>;
                background-color: <?php echo esc_attr($main_color); ?>;
                color: #fff;
            }
            .commentlists .author-name a:hover{
                color: <?php echo esc_attr($main_color); ?>!important;
            }
            .commentlists div.comment_date a,
            .commentlists div.comment_date .fa{
                color: <?php echo esc_attr($main_color); ?>!important;   
            }
            .comment-form textarea:focus, 
            .comment-form input:focus, 
            .content_comments input[type="text"]:focus, 
            .content_comments textarea:focus{
                border-color: <?php echo esc_attr($main_color); ?>;
            }

            footer.footer .scrolltop a:hover i{
                color: <?php echo esc_attr($main_color); ?>;
            }
            .comment-form input[type="submit"]:hover{
                background-color: <?php echo esc_attr($main_color); ?>;   
            }

            .sticky{
                border-top: 5px solid <?php echo esc_attr($main_color); ?>;
            }



            

            @media (max-width: 990px){
                .schedule_single .item .info_left .speaker_info .author a:hover{
                    color: <?php echo esc_attr($main_color); ?>;
                }
                .schedule_single .item .info_left .speaker_info .social a:hover{
                    color: <?php echo esc_attr($main_color); ?>;   
                }
            }


            @media (max-width: 767px){

                header.header ul.sf-menu>li>a,
                header.header .sf-menu li a{
                    color: <?php echo esc_attr($events_cus_menushrink_color); ?>;    
                }



                .dropdown-menu>.active>a,
                header.header ul.sf-menu .dropdown-menu li.current a:hover,
                header .sf-menu .dropdown-menu a:hover,
                header.header ul.sf-menu>li>a:hover, header.header .sf-menu li a:hover{
                    background-color: transparent!important;
                    color: <?php echo esc_attr($main_color); ?>!important;
                }

                header.header {
                    background-color: <?php echo esc_attr($events_cus_bgmenushrink_color); ?>;
                }

                header.header .dropdown-menu{
                    background-color: <?php echo esc_attr($events_cus_bgmenushrink_color); ?>;   
                }
                .af_border{
                    display: none;
                }

            }
                
            
                

         </style>
    <?php
}




/* Google Font */
function events_customize_google_fonts() {
    $fonts_url = '';

    $cusbodyfont = get_theme_mod('events_cus_body_font', 'Open Sans');
    $cusheading = get_theme_mod('events_cus_heading', 'Montserrat');
    $cusbodyfont_c = _x( 'on', $cusbodyfont.': on or off', 'events');
    $cusheading_c = _x( 'on', $cusheading.': on or off', 'events' );

 
    if ( 'off' !== $cusbodyfont_c || 'off' !== $cusheading_c || 'off' !== $cusheading2_c ) {
        $font_families = array();
 
        if ( 'off' !== $cusbodyfont_c ) {
            $font_families[] = $cusbodyfont.':100,200,300,400,500,600,700,800,900"';
        }
 
        if ( 'off' !== $cusheading_c ) {
            $font_families[] = $cusheading.':100,200,300,400,500,600,700,800,900';
        }
        
 
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}


function events_google_fonts() {
    wp_enqueue_style( 'events-fonts', events_customize_google_fonts(), array(), null );
}


function events_add_js($name, $url){
    /* enqueue javascript */
    wp_enqueue_script($name,$url,array('jquery'),false,true);
}   


function events_add_css($name, $url){
    /* enqueue css*/
    wp_enqueue_style( $name,$url);
}

function events_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return $rgb; // returns an array with the rgb values
}


function events_script_ie(){
    /* Fix tf and IE */
    echo '<!--[if lt IE 9]>
    <script src="'.OVA_THEME_URI.'/framework/script/html5shiv.js"></script>
    <script src="'.OVA_THEME_URI.'/framework/script/respond.min.js"></script>
    <![endif]-->';
}

function events_wpadminjs() {

    wp_enqueue_script( 'wpadminjs', OVA_THEME_URI.'/extend/wpadmin.js' );
}
add_action( 'admin_enqueue_scripts', 'events_wpadminjs' );

/* Fix empty */
add_filter( 'the_content', 'events_tgm_io_shortcode_empty_paragraph_fix' );
function events_tgm_io_shortcode_empty_paragraph_fix( $content ) {
 
    $array = array(
        '<p>['    => '[',
        ']</p>'   => ']',
        ']<br />' => ']'
    );
    return strtr( $content, $array );
 
}


