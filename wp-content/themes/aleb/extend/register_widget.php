<?php
// Create sidebar
function events_second_widgets_init() {
  
  $args_blog = array(
    'name' => esc_html__( 'Main Sidebar', 'events'),
    'id' => "main-sidebar",
    'description' => esc_html__( 'Main Sidebar', 'events' ),
    'class' => '',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h4 class="widget-title">',
    'after_title' => "</h4>",
  );
  register_sidebar( $args_blog );

  $footer = array(
    'name' => esc_html__( 'Footer', 'events'),
    'id' => "footer",
    'class' => '',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h4 class="widget-title">',
    'after_title' => "</h4>",
  );
  register_sidebar( $footer );

  

}
add_action( 'widgets_init', 'events_second_widgets_init' );



