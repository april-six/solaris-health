<?php

get_header();

$main_layout = ( ( get_post_meta( get_the_id(), "events_met_main_layout", true ) != 'global') && (get_post_meta( get_the_id(), "events_met_main_layout", true ) != '') ) ? get_post_meta( get_the_id(), "events_met_main_layout", true ) : get_theme_mod( 'events_cus_main_layout', 'right_sidebar' );

$width_sidebar = "col-lg-4 col-md-4 col-sm-12";
$width_main_content = ( $main_layout == "fullwidth" ) ? "col-md-12" : " col-lg-8 col-md-8 col-sm-12 ";

?>
        
<section class="page-section">
    <div class="container">
        <div class="row">

    		<!-- Display sidebar at left  -->
			<?php if($main_layout == "left_sidebar"){ ?>
				<div class="<?php echo esc_attr($width_sidebar); ?>">
					<?php get_sidebar(); ?>
				</div>
			<?php } ?>

			<!-- Display content  -->
			<div class="<?php echo esc_attr($width_main_content); ?>">

				<!-- Display content  -->

		        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		                <?php get_template_part( 'content/content', get_post_format() ); ?>
		                <?php if ( comments_open() ) comments_template( '', true ); ?>
		        <?php endwhile; ?>
		        <?php else : ?>
		                <?php get_template_part( 'content/content', 'none' ); ?>
		        <?php endif; ?>

			</div>

			<!-- Display sidebar at right  -->	
			<?php if($main_layout == "right_sidebar"){ ?>
				<div class="<?php echo esc_attr($width_sidebar); ?>">
					<?php get_sidebar(); ?>
				</div>
			<?php } ?>

        </div>
    </div>
</section>
    
<?php get_footer(); ?>