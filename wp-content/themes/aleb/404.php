<?php get_header(); ?>
<section class="page-section page404a color">
    <div class="container">

        <div id="main-slider">

            <!-- Slide -->
            <div class="item page text-center">
                <div class="caption">
                    <div class="container">
                        <div class="div-table">
                            <div class="div-cell">
                                <div class="caption-subtitle" ><i class="fa fa-warning"></i></div>
                                <h3 class="caption-subtitle"><?php esc_html_e( 'Error 404','events' ); ?></h3>
                                <h2 class="caption-title"><?php esc_html_e( 'Page not Found', 'events' ); ?></h2>
                                <p class="caption-text">
                                    <a class="btn btn-theme btn-theme-xl scroll-to" href="<?php echo esc_url( home_url( '/' ) );  ?>" > <?php esc_html_e('Go to Homepage','events') ?> <i class="fa fa-arrow-circle-right"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

	</div>
</section>

<?php get_footer(); ?>