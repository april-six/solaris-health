<?php

get_header();

$main_layout = ( ( get_post_meta( get_the_id(), "events_met_main_layout", true ) != 'global') && (get_post_meta( get_the_id(), "events_met_main_layout", true ) != '') ) ? get_post_meta( get_the_id(), "events_met_main_layout", true ) : get_theme_mod( 'events_cus_main_layout', 'right_sidebar' );

$width_sidebar = "col-lg-4 col-md-4 col-sm-12";
$width_main_content = ( $main_layout == "fullwidth" ) ? "col-md-12" : " col-lg-8 col-md-8 col-sm-12 ";

?>
        
<section class="page-section">
    <div class="container">
        <div class="row">

    		<!-- Display sidebar at left  -->
			<?php if($main_layout == "left_sidebar"){ ?>
				<div class="<?php echo esc_attr($width_sidebar); ?>">
					<?php get_sidebar(); ?>
				</div>
			<?php } ?>

			<!-- Display content  -->
			<div class="<?php echo esc_attr($width_main_content); ?>">
				<div class="row schedule_single">
					<?php  if(have_posts()) : while(have_posts()) : the_post(); ?>
								
								
									<?php

									
									$html = '';
									
								  $thumbnail_url = wp_get_attachment_url(get_post_thumbnail_id());
                                  $schedule_timetext = get_post_meta($post->ID, "events_met_schedule_timetext", true);
                                  $schedule_timeicon = get_post_meta($post->ID, "events_met_schedule_timeicon", true);
                                  $speaker_intermediate = get_post_meta($post->ID, "events_met_schedule_intermediate", true);
                                  $speaker_intermediate_icon = get_post_meta($post->ID, "events_met_speaker_intermediate_icon", true);

                                  $schedule_author = get_post_meta($post->ID, "events_met_schedule_author", true);
                                  $schedule_author_job = get_post_meta($post->ID, "events_met_schedule_author_job", true);
                                  $schedule_link_speaker = get_post_meta($post->ID, "events_met_schedule_link_speaker", true);

                                 
								  
									$html .= '
									<div class="schedule_timeline ">
									  <article class="item" style="background-color: transparent;">
                                                
                                                  <div class="col-md-5 info_left">';
                                                    $html .= '<div class="thumbnail_spe">
                                                                <img src="'.$thumbnail_url.'" alt="'.$schedule_author.'" title="'.$schedule_author.'" class="img-responsive" />
                                                                <div class="speaker_info">';
                                                                  if($schedule_link_speaker){
                                                                    $html .= ($schedule_author!='') ? '<div class="author"><a href="'.$schedule_link_speaker.'">'.$schedule_author.'</a></div>' : '';
                                                                  }else{
                                                                    $html .= ($schedule_author!='') ? '<div class="author">'.$schedule_author.'</div>' : '';
                                                                  }
                                                                  
                                                                  $html .= ($schedule_author_job!='') ? '<div class="job">'.$schedule_author_job.'</div>' : '';
                                                                  
                                                                  
                                                                $html .= '</div>
                                                              </div>';

                                                  $html.='</div>
                                                  <div class="col-md-7 info_right">';

                                                	$cats = wp_get_post_terms($post->ID, 'categories');
                                                	$html .= '<div class="room_time">';
                                                	if(count($cats) == 1){
                                                		$html .= '<h3 class="cat_parent">'.$cats[0]->name.'</h3>';
                                                	}else{
                                                		foreach($cats as $cat){
                                                			if($cat->parent == 0){
                                                				$html .= '<h3 class="cat_parent">'.$cat->name.'</h3>';		
                                                			}
                                                		}
                                                		foreach($cats as $cat){
                                                			if($cat->parent != 0){
                                                				$html .= '<div class="cat_child">'.$cat->name.'</div>';
                                                			}
                                                		}
                                                	}
                                                	$html .= '</div>';
									
                                                    
                                                    $html .= '<h2 class="post-title">'.get_the_title( ).'</h2>';
                                                    

                                                    $html .='<div class="quick_speaker">';
	                                                  $html .= '<div class="time">';
	                                                  $html .= ($schedule_timeicon != '') ? '<i class="fa '.$schedule_timeicon.'"></i>&nbsp;&nbsp;':'';
	                                                  $html .= ($schedule_timetext != '') ? $schedule_timetext:'';
	                                                  $html .= '</div>';
	                                                  $html .= '<div class="intermediate">';
	                                                  $html .= ($speaker_intermediate_icon != '') ? '<i class="fa '.$speaker_intermediate_icon.'"></i>&nbsp;&nbsp;':'';
	                                                  $html .= ($speaker_intermediate != '') ? $speaker_intermediate : '';
	                                                  $html .= '</div>';

                                                    $html .= '</div>';
                                                    
                                                  $html .= '</div>

                                              </article></div>'; 
									  echo wp_kses($html,true);  
									?>
								
								
								
								
									<div style="clear:both;"></div>

		                            <article class="content_schedule">
										
										
											
											<div class="container-fluid">
												<?php the_content();
													wp_link_pages();                
												?>
											</div>
										
									</article>
					
	                        <?php endwhile; ?>
	                		<?php else: ?>
	                    		<h1><?php esc_html_e('Nothing Found Here!', 'events'); ?></h1>
	                <?php endif; ?>	  
                </div>
			</div>

			<!-- Display sidebar at right  -->	
			<?php if($main_layout == "right_sidebar"){ ?>
				<div class="<?php echo esc_attr($width_sidebar); ?>">
					<?php get_sidebar(); ?>
				</div>
			<?php } ?>

        </div>
    </div>
</section>
    
<?php get_footer(); ?>