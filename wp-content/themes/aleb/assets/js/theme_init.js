(function($) {
    
    'use strict';

    $(document).ready(function () {

        "use strict";

        theme.init();
        theme.sc_button();
        theme.mainslider();
        theme.countdown();
        theme.onepagemenu();
        theme.frequently_questions_slider();
        theme.speakers();
        theme.sponsor();
        theme.bgslide();
        theme.twitterst();
        theme.testimonials();
        theme.blogs();
        theme.googlemap();
        theme.twitter();

        $(window).load(function(){
        
            var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
            if (isMobile == false) {
                $('*[data-animation]').addClass('animated');
                $('.animated').waypoint(function (down) {
                    var el = $(this);
                    var animation = el.data('animation');
                    if (!el.hasClass('visible')) {
                        var animationDelay = el.data('animation_delay');
                        if (animationDelay) {
                            setTimeout(function () {
                                el.addClass(animation + ' visible');
                            }, animationDelay);
                        } else {
                            el.addClass(animation + ' visible');
                        }
                    }
                }, {
                    offset: '100%'
                });
            }

          
                    
                
        });


    });


    $(window).load(function() {
        $('#loading').fadeOut();
    });


    $(document).ready(function () {
        "use strict";
        theme.onResize();
    });
    $(window).load(function () {
        theme.onResize();
    });
    $(window).resize(function () {
        theme.onResize();
    });


    window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);
     
      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };
     
      return t;
    }(document, "script", "twitter-wjs"));

})(jQuery);

