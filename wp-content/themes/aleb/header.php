<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <?php wp_head(); ?>
</head>

<?php


// Get version layout in each page or post
$events_met_version_layout = (get_post_meta($wp_query->get_queried_object_id(), "events_met_version_layout", true) != 'global') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_version_layout", true) : get_theme_mod("events_cus_version_layout", 'wide');
$page_text_direction = (is_rtl()) ? 'rtl' : 'ltr';

$header_style = (get_post_meta($wp_query->get_queried_object_id(), "events_met_header_style", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_header_style", true) : 'header_default';

$bg = (get_post_meta($wp_query->get_queried_object_id(), "events_met_general_bg_heading", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_general_bg_heading", true) : '';
$title = get_post_meta($wp_query->get_queried_object_id(), "events_met_general_title_heading", true);
$subtitle = get_post_meta($wp_query->get_queried_object_id(), "events_met_general_subtitle_heading", true);

if (is_single() == true) {
    $title = get_the_title();
    $subtitle = get_the_time(get_option('date_format'), $wp_query->get_queried_object_id());
}

if ($header_style == 'header_default') {
    $bgcolor_menu = (get_post_meta($wp_query->get_queried_object_id(), "events_met_bgcolor_menu", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_bgcolor_menu", true) : '#000000';
} else if ($header_style == 'header_transparent') {
    $bgcolor_menu = 'transparent';
} else {
    $bgcolor_menu = (get_post_meta($wp_query->get_queried_object_id(), "events_met_bgcolor_menu", true) != '') ? get_post_meta($wp_query->get_queried_object_id(), "events_met_bgcolor_menu", true) : '';
}


?>

<body <?php body_class($events_met_version_layout . ' ' . $page_text_direction . ' ' . $header_style); ?> >

    <!-- loading -->
    <?php if (get_theme_mod("events_cus_display_spin", "yes") != "no") { ?>
        <div id="loading">
            <div class="timer-loader">
              <?php esc_html_e('Loading...', 'events'); ?>
            </div>    
        </div>
    <?php 
} ?>
    <!-- /loading -->

    <div class="container_boxed">
        <div class="wrapper">

                <?php if ($header_style == 'header_bg') { ?>
                    <div class="bg_heading" style="background: url(<?php echo esc_url($bg); ?>);">
                        <div class="bg_mask"></div>
                <?php 
            } ?>

                    <!-- header -->
                    <header class="header <?php echo esc_attr($header_style); ?> " style="<?php echo ($bgcolor_menu != "") ? "background-color:" . esc_attr($bgcolor_menu) : ''; ?>">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="logo">
                                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
                                            <img src="<?php echo esc_url(get_theme_mod('events_cus_logo_image', OVA_THEME_URI . '/framework/customizer/images/logo.png')); ?>" alt="<?php echo esc_attr(bloginfo('name')); ?>"/>
                                        </a>
                                    </div>
                                    
                                    <nav class="menutop navbar navbar-default ">
                                       
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu_top" aria-expanded="false">
                                                    <span class="sr-only"><?php esc_html_e('Toggle navigation', 'events'); ?></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                                
                                                <a class="navbar-brand logo_image" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
                                                        <img src="<?php echo esc_url(get_theme_mod('events_cus_logoshrink_image', OVA_THEME_URI . '/framework/customizer/images/logo_shrink.png')); ?>" alt="<?php echo esc_attr(bloginfo('name')); ?>"/>    
                                                </a>
                                            </div>

                                            <div class="collapse navbar-collapse" id="menu_top">
                                            <?php
                                            $themelocation = get_post_meta($wp_query->get_queried_object_id(), "events_met_location_theme", true) ? get_post_meta($wp_query->get_queried_object_id(), "events_met_location_theme", true) : 'primary';
                                            wp_nav_menu(array(
                                                'menu' => '',
                                                'theme_location' => $themelocation,
                                                'depth' => 3,
                                                'container' => '',
                                                'container_class' => '',
                                                'container_id' => '',
                                                'menu_class' => 'sf-menu nav',
                                                'link_after' => '<span class="af_border"></span>',
                                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                                'walker' => new wp_bootstrap_navwalker()
                                            ));
                                            ?>
                                            </div>
                                        
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- header -->

                <?php if ($header_style == 'header_bg') { ?>    
                        <div class="heading_bg events_heading text-center">
                            <h2><?php echo wp_kses($title, true); ?></h2>
                            <?php if ($subtitle) { ?>
                            <h3><?php echo wp_kses($subtitle, true); ?></h3>
                            <?php 
                        } ?>
                            <hr>
                        </div>
                    </div>
                <?php 
            } ?>

            

            

        
            
            

            